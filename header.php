<header class="blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div id="logo">HealthPro</div>
            </div>
            <div class="col-sm-9 desktop-menu">
                <ul class="nav">
                    <ul class="nav">
                        <a href="/index.php">
                            <li>Home <i class="ion-home"></i></li>
                        </a>
                        <a href="/courses.php">
                            <li>Courses</li>
                        </a>
                        <a href="/services.php">
                            <li>Services</li>
                        </a>
                        <a href="/faqs.php">
                            <li>FAQs</li>
                        </a>
                        <a href="#">
                            <li>
                                Partners
                                <ul class="dropdown">
                                    <a href="/corporates.php">
                                        <li>Corporates</li>
                                    </a>
                                    <a href="/training_centers.php">
                                        <li>Training Centers</li>
                                    </a>
                                    <a href="/instructors.php">
                                        <li>Instructors</li>
                                    </a>
                                </ul>
                            </li>
                        </a>
                        <?php
                        error_reporting(0);
                        if (!isset($_SESSION["user_manager"])) {
                            echo "<a href='#sign-in-modal' class='sign-in'><li>Sign In</li></a>";
                        } else {
                            echo "<a href='/profile.php' class='sign-in'><li>My Account</li></a>";
                        }
                        ?>
                    </ul>
                </ul>
            </div>

            <div class="mobile-menu">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    </div>
</header>

<div class="remodal" data-remodal-id="sign-in-modal">
    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="col-sm-6 col-sm-offset-3">
        <h2>Welcome back !</h2>
        <p class="subtext">Please enter your sign in details</p>


        <form class="edit-profile-form" method="post" action="login.php">

            <input name="email" type="email" class="form-control input" placeholder="Email Address">

            <input name="password" type="password" class="form-control input" placeholder="Password">

            <input name="loginButton" type="submit" value="Sign In" class="button full-width"/>

            <p>&nbsp;</p>
            <a href="sign_up.php">or sign up here</a>
            <p>&nbsp;</p>
            <a href="forgot.php">Forgot Password?</a>
        </form>
    </div>
</div>

        