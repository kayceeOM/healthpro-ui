<header class="blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div id="logo">HealthPro</div>
            </div>
            <div class="col-sm-9">
                <ul class="nav">
                    <a href="/courses.php">
                        <li>Courses</li>
                    </a>
                    <a href="/profile.php">
                        <li>My Profile</li>
                    </a>
                    <a href="/signout.php" class="sign-out">
                        <li>Sign Out <i class="ion-log-out"></i></li>
                    </a>
                </ul>
            </div>
        </div>
    </div>
</header>