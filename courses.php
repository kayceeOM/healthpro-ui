<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#results").load("fetch_pages.php"); //load initial records
            //executes code below when user click on pagination links
            $("#results").on("click", ".pagination a", function (e) {
                e.preventDefault();
                $(".loading-div").show(); //show loading element
                var page = $(this).attr("data-page"); //get page number from link
                $("#results").load("fetch_pages.php", {"page": page}, function () { //get content from PHP page
                    $(".loading-div").hide(); //once done, hide loading element
                });

            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php
    session_start();
    include("header.php"); ?>

    <section id="courses">
        <div class="container">
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>All Courses</h2>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <form action="search.php" method="post" class="search-form">
                        <div class="search-group">
                            <input name="course_query" type="text" class="form-control search-input"
                                   placeholder="Search for courses ...">
                            <button class="button icon-button"><i class="ion-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END First Row-->

            <div id="results" class="row courses-row"></div>
            <!-- <div class="loading-div"><img src="ajax-loader.gif" ></div> -->
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>