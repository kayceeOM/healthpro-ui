<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php
    session_start();
    include("header.php"); ?>


    <section id="services">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Training Centers</h2>

                <div class="services">

                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="service">
                            <p>With over 43 training location nation-wide, you search through our list of courses for an
                                Applegreen Learning Center near you.</p>

                            <p>For more information on this, please mail to learning@applegreenconsult.com
                                indicating:</p>
                            <ul>
                                <li>The courses or subject matter in which you are interested</li>
                                <li>Desired dates for your training</li>
                                <li>Your preferred location</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>