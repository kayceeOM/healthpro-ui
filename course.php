<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
if (!isset($_SESSION["user_manager"])) {
    $_SESSION['header'] = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}/{$_SERVER['REQUEST_URI']}";
    echo " <script>window.location='login.php';</script>";
}
?>

<?php
$share = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}/{$_SERVER['REQUEST_URI']}";
if (isset($_GET['a'])) {
    # code...
    $username = decrypt($_SESSION["user_manager"]);
    $course_id = decrypt($_GET['a']);

    $venues = "";

    $query = "SELECT * FROM courses where id = $course_id";
    $result = $conn->query($query);
    //$number = $result->num_rows;
    if ($result === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
    } else {

        $modal = '

                        <div class="remodal" data-remodal-id="enroll-modal">
                            <button data-remodal-action="close" class="remodal-close"></button>
                            <div class="col-sm-6 col-sm-offset-3">
                                <h2>Course Enrollment</h2>
                                <p class="subtext">Choose your preferred location.</p>

                                <form class="sign-in-form" method="post" action="enroll.php">
                                    <select name="location" class="form-control input">
                                        
                    ';
        $result->data_seek(0);
        while ($row = $result->fetch_assoc()) {
            $id = $row["id"];
            $name = $row["name"];
            $type = $row["type"];
            $training_loc = $row["training_loc"];
            $cost = $row["cost"];
            $prof_point = $row["prof_point"];
            $description = $row["description"];
            $image = $row["image"];

            if ($type == 'both') {
                # code...
                $type = 'online and classroom';
            }

            $count = 0;
            $training_loc = json_decode($training_loc, true);
            foreach ($training_loc as $location) {
                $location = explode("||", $location);
                $loc_name = $location[0];
                $loc_address = $location[1];

                $query2 = "SELECT * FROM training_loc where id = $loc_name";
                $result2 = $conn->query($query2);
                $result2->data_seek(0);
                while ($row2 = $result2->fetch_assoc()) {
                    $loc_name = $row2["name"];
                    $loc_address = $row2["location"];
                }
                $count++;
                $modal .= '
                                        <option value="' . $loc_name . '">' . $loc_name . '</option>
                                ';
                $venues .= '
                                    <div class="col-md-3">
                                        <div class="venue-item">         
                                            <div class="details">
                                                <h3>Venue ' . $count . '</h3>
                                                <h4>' . $loc_name . '</h4>
                                                <div class="venue__address">' . $loc_address . '</div>
                                                
                                                <!-- <div class="venue__instructor">
                                                    <h4 class="instructor__heading">Instructor(s)</h4>
                                                    <div class="instructor__name">Olayemi Chukwudi</div>
                                                    <div class="instructor__name">Olayemi Chukwudi</div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                            ';

            }

            $modal .= '
                                    <option value="Online">Online</option>
                                 </select>
                                    <input name="prof_point" type="hidden" value="' . $prof_point . '">
                                    <input name="course_name" type="hidden" value="' . $name . '">
                                    <input name="course_id" type="hidden" value="' . $id . '">
                                    <input name="cost" type="hidden" value="' . $cost . '">
                                    <input type="submit" class="button full-width" value="Proceed to Payment" />
                                </form>
                            </div>
                        </div>
                        ';

        }
    }

    $result->free();

}

?>


<?php

$query2 = "SELECT * FROM classroom where course_id = $course_id and user_id='$username' and payment_status='paid'";
$already_registered = "";
$result2 = $conn->query($query2);
$result2->data_seek(0);
if ($result2->num_rows > 0) {
    $already_registered = '
                                        <div class="col-sm-4">
                                            <div class="button-wrap">
                                                <a href="classroom.php?a=' . encrypt($course_id) . '" class="button full-width big blue">Go to classroom <i class="ion-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    ';

} else {
    $already_registered = '
                                        <div class="col-sm-4">
                                            <div class="button-wrap">
                                                <a href="#enroll-modal" class="button full-width big blue">Enroll for this Course <i class="ion-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    ';
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<div class="remodal-bg">
    <?php include("header.php"); ?>
    <section id="course-hero"
             style="background-image: linear-gradient(-180deg, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.7) 100%),url(course_icons/<?= $image ?>);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="course-details">
                        <h1 class="course-title"><?= $name ?></h1>
                        <p class="course-desc"><?= $description ?>.</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="more-details">
                            <div class="price"><i class="ion-cash"></i><?= $cost ?></div>
                            <div class="location"><i class="ion-ios-location"></i><?= $type; ?></div>
                            <!--<div class="duration"><i class="ion-calendar"></i>Sept 12 – Feb 13, 2016.</div>-->
                        </div>
                    </div>
                    <?= $already_registered; ?>
                    <!-- <div class="col-sm-4">
                        <div class="button-wrap">
                            <a href="#enroll-modal" class="button full-width big blue">Enroll for this Course <i class="ion-chevron-right"></i></a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>

    <section id="courses" class="course-page">
        <div class="container">
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>Venues</h2>
                </div>
                <div class="col-md-3">
                    <a href="#" class="button disabled"><?= $count ?> Venues</a>
                </div>
            </div>
            <!-- END First Row-->

            <div class="row venues-row">

                <?= $venues; ?>

                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>Similar Courses</h2>
                </div>
                <div class="col-md-3">
                    <a href="courses.php" class="button">View All Courses <i class="ion-ios-arrow-right"></i></a>
                    <!-- <a href="#" class="button disabled">4 Courses</a> -->
                </div>
            </div>
            <!-- END First Row-->

            <div class="row courses-row">
                <?php
                $results = $mysqli->prepare("SELECT id, name, image, description FROM courses where status='active' and category='$category' and id != '$course_id' ORDER BY id DESC LIMIT 4");
                $results->execute(); //Execute prepared Query
                $results->bind_result($id, $name, $image, $description); //bind variables to prepared statement

                $description = strip_tags($description);

                if (strlen($description) > 500) {

                    // truncate string
                    $descriptionCut = substr($description, 0, 500);

                    // make sure it ends in a word so assassinate doesn't become ass...
                    $description = substr($descriptionCut, 0, strrpos($descriptionCut, ' ')) . '...';
                }
                //Display records fetched from database.

                while ($results->fetch()) { //fetch values
                    echo '<div class="col-md-3">
                                            <a href="course.php?a=' . encrypt($id) . '">
                                            <div class="course-card">
                                                <div class="image" style="background-image: url(course_icons/' . $image . ')">
                                                    
                                                </div>
                                                <div class="details">
                                                    <h3>' . $name . '</h3>
                                                    <p>' . $description . '</p>
                                                </div>
                                            </div>
                                            </a>
                                        </div>';
                }
                ?>

            </div>
        </div>
    </section>
    <section id="social-share">
        <div class="container">
            <h2 class="social-share__title">Share this course</h2>
            <ul class="social-share__sites">
                <li class="share-site twitter">
                    <a href="https://twitter.com/share?url=<?= $share ?>&amp;text=HealthPro&amp;hashtags=HealthPro"
                       target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="share-site facebook">
                    <a href="http://www.facebook.com/sharer.php?u=<?= $share ?>" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li class="share-site google-plus">
                    <a href="https://plus.google.com/share?url=<?= $share ?>" target="_blank">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
                <li class="share-site linkedin">
                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?= $share ?>">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <?= $modal; ?>
    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>