<section id="footer">
    <div class="container">
        <div class="row links">
            <div class="col-md-8 left no-pad">
                <a href="#" class="link">Services</a>
                <a href="#" class="link">Payment Options</a>
                <a href="#" class="link">FAQs</a>
                <a href="#" class="link">Resources</a>
                <a href="/admin" class="link">Admin Login</a>
            </div>
            <div class="col-md-4 right no-pad">
                &copy; Copyright &bull; HealthPro, 2016.
            </div>
        </div>
    </div>
</section>

        