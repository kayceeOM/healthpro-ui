<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();

?>

<?php
$reply = '';
// Parse the log in form if the user has filled it out and pressed "Log In"
if (isset($_POST["email"]) && isset($_POST["password"])) {

    $manager = $_POST["email"]; // filter everything but numbers and letters
    $password = $_POST["password"]; // filter everything but numbers and letters
    $pass = md5($password);
    // Connect to the MySQL database  

    $sql = mysqli_query($conn, "SELECT * FROM account WHERE email='$manager' AND password='$pass' LIMIT 1"); // query the person
    // ------- MAKE SURE PERSON EXISTS IN DATABASE ---------
    $existCount = mysqli_num_rows($sql); // count the row nums
    if ($existCount == 1) { // evaluate the count
        while ($row = mysqli_fetch_array($sql)) {
            $id = $row["id"];
        }
        $_SESSION["user_manager"] = encrypt($manager);
        echo '' . (isset($_SESSION['header']) ? '<script>window.location="' . $_SESSION['header'] . '"</script>' : '<script>window.location="profile.php"</script>');

        exit();
    } else {
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Username/Password is not correct
    </div>
    ';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<?php include("header.php"); ?>
<section id="profile-edit" class="sign-up-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="edit-profile-card">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center">Sign In</h2>
                            <form class="edit-profile-form text-center" method="post" action="login.php">
                                <?php echo $reply; ?>
                                <input name="email" type="email" class="form-control input" placeholder="Email Address">

                                <input name="password" type="password" class="form-control input"
                                       placeholder="Password">

                                <input name="loginButton" type="submit" value="Sign In" class="button full-width"/>

                                <p>&nbsp;</p>
                                <a class="text-center" href="sign_up.php">or sign up here</a>
                                <p>&nbsp;</p>
                                <a href="forgot.php">Forgot Password?</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END First Row-->

    </div>
</section>

<?php include("footer-min.php"); ?>
</body>
</html>