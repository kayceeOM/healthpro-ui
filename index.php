<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php include("header.php"); ?>

    <section id="hero">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">

                <div class="hero-details">
                    <h1 class="hero-text" id="byline"></h1>
                    <div class="col-md-8 col-md-offset-2">
                        <p class="hero-desc" id="desc">We will continuously challenge the status quo, making healthcare
                            accessible and pleasant for you</p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="calls-to-action" id="cta">
                    <a href="courses.php" class="button blue big">View Courses</a>
                    <a href="services.php" class="button clear big">Learn More</a>
                </div>
            </div>
        </div>
    </section>

    <section id="why-us">
        <div class="container">
            <div class="row">
                <!-- Advantages -->
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="advantage">
                                <div class="icon">
                                    <i class="ion-unlocked"></i>
                                </div>
                                <h3>Access</h3>
                                <p>Our lecture notes, modules and other learning materials are easily and instantly
                                    accessible at your convenience.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="advantage">
                                <div class="icon">
                                    <i class="ion-upload"></i>
                                </div>
                                <h3>Updates</h3>
                                <p>The remote and dynamic nature of our learning infrastructure allows for on demand,
                                    easily implemented updates to our curricula.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="advantage">
                                <div class="icon">
                                    <i class="ion-cash"></i>
                                </div>
                                <h3>Cost</h3>
                                <p>Taking our classes costs a fraction of what it would cost to run the trainings
                                    otherwise, and we offer a flexible approach depending on you.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="advantage">
                                <div class="icon">
                                    <i class="ion-ios-folder"></i>
                                </div>
                                <h3>Database</h3>
                                <p>You can access our vast searchable database of learning materials, where you can turn
                                    to when you have questions and inquiries.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Advantages -->

                <!-- About -->
                <div class="col-md-6">
                    <div class="about">
                        <h2>About Our Training</h2>
                        <p>We provide training and continuous education for healthcare professionals and
                            organizations.</p>
                        <p>Some of courses are offered in partnership with reputable organizations and experts which
                            adhere strictly to professional and ethical standards.</p>
                        <p>Most of our training is classroom-based, some of course offerings are online depending on our
                            students’ choices and availability of materials.</p>
                        <div class="clearfix"></div>
                        <div class="button-wrap">
                            <a href="#" class="button">Learn More <i class="ion-ios-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- END About -->
            </div>
        </div>
    </section>

    <section id="featured-courses">
        <div class="container">
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>Featured Courses</h2>
                </div>
                <div class="col-md-3">
                    <a href="/courses.php" class="button">View All Courses <i class="ion-ios-arrow-right"></i></a>
                </div>
                <div class="col-md-6">
                    <form action="search.php" method="post" class="search-form">
                        <div class="search-group">
                            <input name="course_query" type="text" class="form-control search-input"
                                   placeholder="Search for courses ...">
                            <button class="button icon-button"><i class="ion-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END First Row-->

            <div class="row courses-row">
                <?php
                $results = $mysqli->prepare("SELECT id, name, image, description FROM courses where status='active' ORDER BY id DESC LIMIT 4");
                $results->execute(); //Execute prepared Query
                $results->bind_result($id, $name, $image, $description); //bind variables to prepared statement

                $description = strip_tags($description);

                if (strlen($description) > 500) {

                    // truncate string
                    $descriptionCut = substr($description, 0, 500);

                    // make sure it ends in a word so assassinate doesn't become ass...
                    $description = substr($descriptionCut, 0, strrpos($descriptionCut, ' ')) . '...';
                }
                //Display records fetched from database.

                while ($results->fetch()) { //fetch values
                    echo '<div class="col-md-3">
                                            <a href="course.php?a=' . encrypt($id) . '">
                                            <div class="course-card">
                                                <div class="image" style="background-image: url(course_icons/' . $image . ')">
                                                    
                                                </div>
                                                <div class="details">
                                                    <h3>' . $name . '</h3>
                                                    <p>' . $description . '</p>
                                                </div>
                                            </div>
                                            </a>
                                        </div>';
                }
                ?>

            </div>
        </div>
    </section>

    <section id="stats">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <h2 class="text-center">Our Stats</h2>

                <div class="clearfix"></div>
                <div class="col-md-3">
                    <div class="stat">
                        <div class="icon"><i class="ion-map"></i></div>
                        <div class="number">35</div>
                        <div class="detail">Training</br>Locations</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="stat">
                        <div class="icon"><i class="ion-person-stalker"></i></div>
                        <div class="number">2,343</div>
                        <div class="detail">Trained</br>Professionals</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="stat">
                        <div class="icon"><i class="ion-android-laptop"></i></div>
                        <div class="number">121</div>
                        <div class="detail">Training</br>Courses</div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="stat">
                        <div class="icon"><i class="ion-easel"></i></div>
                        <div class="number">43</div>
                        <div class="detail">Professional</br>Trainers</div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<div id="clients">
    <div class="container">
        <h2>Our Clients</h2>
        <div class="row">
            <img src="img/clients/1-chevron.png" class="client">
            <img src="img/clients/2-cbn.png" class="client">
            <img src="img/clients/3-corona.png" class="client">
            <img src="img/clients/4-delsuth.png" class="client">
            <img src="img/clients/5-russel-smith.png" class="client">
            <img src="img/clients/6-premium-health.png" class="client">
            <img src="img/clients/7-union-bank.png" class="client">
            <img src="img/clients/8-ncpp.png" class="client">
            <img src="img/clients/9-wagpc.png" class="client">
        </div>
    </div>
</div>

<?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
<!-- <script src="js/main.js"></script> -->

<script>
    var onload = function () {
        var timeouts = [];
        var text = "World-Class Training for Healthcare Professionals";
        var byline = document.getElementById('byline');

        var delay_time = 0;
        text.split('').forEach(function (letter, i) {
            delay_time += 40;
            timeouts.push(setTimeout(function () {
                byline.innerHTML = byline.innerHTML + letter;
                if ((i + 1) == text.length) {
                    document.getElementById('desc').style.opacity = 1;
                    document.getElementById('cta').style.opacity = 1;
                }
            }, delay_time));
        });
    }
</script>
</body>
</html>