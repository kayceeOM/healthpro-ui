<?php
include_once('storescripts/connect_to_mysql.php');
include_once('storescripts/crypto.php');
$reply = '';
session_start();
if (isset($_SESSION["user_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}

if (isset($_POST["regButton"])) {
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $pass = $_POST["pass"];
    $pass2 = $_POST["pass2"];
    $email = $_POST["email"];
    $phoneno = $_POST["phone"];

    if ($pass == $pass2) {
        $sql = "SELECT * FROM user WHERE email='$email'";
        $insert_pro = mysqli_query($conn, $sql);
        $existCount = mysqli_affected_rows($conn); // count the row nums
        if ($existCount >= 1) { // evaluate the count
            $reply = '
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Sorry! email has already been registered 
        </div>
        ';
        } else {
            $password = md5($pass);

            $image = $_FILES['image']['name'];

            // Connect to the MySQL database
            $insert_user = "insert into account (firstname, lastname, email, password, phone, image, credit_points, date_created) values('$fname', '$lname', '$email', '$password', '$phoneno', '$image', '0', now())";
            // insert into the database
            $insert_pro = mysqli_query($conn, $insert_user) or die(mysqli_error($conn));

            if ($insert_pro) {

                $image_temp1 = $_FILES['image']['tmp_name'];
                move_uploaded_file($image_temp1, "img/users/$image");

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                $headers .= "From: HealthPro <support@Healthpro.com>" . "\r\n";
                // message subject
                $subject = 'Welcome to HealthPro';
                $fullname = $fname . " " . $lname;
                // Forming Message
                $text = "hi $fullname \n </br>
                        Thank you for registering with us. your registration details are:
                        </br></br>
                        Email: $email
                        Password: $pass

                        if you have any questions, pls contact us at support@Healthpro.com
                        ";

                $result = mail($email, $subject, $text, $headers);

                echo "<script>window.open('reg_success.php','_self')</script>";

            } else {
                $reply = '
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                An Error occured during registration, Pls contact support 
            </div>
            ';
            }//if user was not inserted properly


        }//if email hs not been used


    }//if passwords match
    else {
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Sorry! Passwords do not match 
    </div>
    ';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<?php include("header.php"); ?>
<section id="profile-edit" class="sign-up-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="edit-profile-card">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center">Sign Up</h2>
                            <form name="sign-up-form" method="post" enctype="multipart/form-data" action="sign_up.php"
                                  class="edit-profile-form">
                                <?php echo $reply; ?>
                                <div class="form-group">
                                    <label>Profile Image</label>
                                    <input name="image" accept="image/*" type="file" class="form-control input">
                                </div>

                                <input name="fname" type="text" class="form-control input" placeholder="First Name">

                                <input name="lname" type="text" class="form-control input" placeholder="Last Name">

                                <input name="email" type="email" class="form-control input" placeholder="Email Address">

                                <input name="phone" type="phone" class="form-control input" placeholder="Phone Number">

                                <input name="pass" type="password" class="form-control input" placeholder="Password">

                                <input name="pass2" type="password" class="form-control input"
                                       placeholder="Confirm Password">

                                <input name="regButton" type="submit" value="Sign Up" class="button full-width"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END First Row-->

    </div>
</section>

<?php include("footer-min.php"); ?>
</body>
</html>