<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
$reply = '';
if (!isset($_SESSION["user_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}

$email = decrypt($_SESSION["user_manager"]);

$results = $mysqli->prepare("SELECT id, firstname, lastname, email, phone, image, credit_points FROM account where email='$email'");
$results->execute(); //Execute prepared Query
$results->bind_result($id, $firstname, $lastname, $email, $phone, $image, $credit_points); //bind variables to prepared statement

while ($results->fetch()) { //fetch values

}


if (isset($_POST["UpdateButton"])) {
    $id = $_POST['id'];
    $imageicon = $_POST['imageicon'];
    $image = $_FILES['image']['name'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $pass = $_POST['pass'];
    $pass2 = $_POST['pass2'];
    # code...
    if ($image == '') {
        $image = $imageicon;
    } else {
        //$image = $_FILES['image']['name'];
        $image_temp1 = $_FILES['image']['tmp_name'];
        move_uploaded_file($image_temp1, "../game_icons/$image");
    }

    if ($pass == "" && $pass2 == "") {
        # code...
        $stmt = $mysqli->prepare("UPDATE account SET firstname = ?, lastname = ?, email = ?,  phone = ?,  image = ?  WHERE id = ?");
        $stmt->bind_param('sssssi', $firstname, $lastname, $email, $phone, $image, $id);
        if ($stmt->execute()) {
            # code...
            $reply = '
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Your account has been updated 
    </div>
    ';
        }
    } else {
        if ($pass == $pass2) {
            # code...
            $stmt = $mysqli->prepare("UPDATE account SET firstname = ?, lastname = ?, email = ?, password = ?,  phone = ?,  image = ?  WHERE id = ?");
            $stmt->bind_param('sssssi', $firstname, $lastname, $email, $password, $phone, $image, $id);
            if ($stmt->execute()) {
                # code...
                $reply = '
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Your account has been updated 
    </div>
    ';
            }
        } else {
            $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Sorry! Passwords do not match 
    </div>
    ';
        }
    }


}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<?php include("profile_header.php"); ?>

<section id="profile-hero" class="edit" style="background-image: url(img/profile-bg.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="profile-details">
            <div class="profile-image" style="background-image: url(img/users/<?php echo $image ?>)"></div>
        </div>
    </div>
</section>

<section id="profile-edit" class="course-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="edit-profile-card">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center">Edit Profile</h2>
                            <form name="edit-profile-form" method="post" enctype="multipart/form-data"
                                  action="profile_edit.php" class="edit-profile-form">
                                <?php echo $reply; ?>
                                <div class="form-group">
                                    <label>Profile Image</label>
                                    <input name="id" type="hidden" class="form-control input" value="<?= $id ?>">
                                    <input name="imageicon" type="hidden" class="form-control input"
                                           value="<?= $image ?>">
                                    <input name="image" type="file" class="form-control input"
                                           placeholder="Profile Image">
                                </div>
                                <input name="fname" type="text" class="form-control input" value="<?= $firstname ?>">
                                <input name="lname" type="text" class="form-control input" value="<?= $lastname ?>">
                                <input name="email" type="email" class="form-control input" value="<?= $email ?>">
                                <input name="phone" type="phone" class="form-control input" value="<?= $phone ?>">
                                <input name="pass" type="password" class="form-control input"
                                       placeholder="Password (Optional)">
                                <input name="pass2" type="password" class="form-control input"
                                       placeholder="Confirm Password (Optional)">
                                <input name="UpdateButton" type="submit" value="Save Changes"
                                       class="button full-width"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END First Row-->

    </div>
</section>

<?php include("footer-min.php"); ?>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>