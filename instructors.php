<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php
    session_start();
    include("header.php"); ?>


    <section id="services">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Instructors</h2>

                <div class="services">

                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="service text-center">
                            <p>Our instructors are recognized thought leaders and experts in their respective fields of
                                study. With years of field experience and instruction under their belt, coupled with a
                                healthy dose of dedication, they have been vetted and handpicked by our team to deliver
                                on some of the best training models around.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>