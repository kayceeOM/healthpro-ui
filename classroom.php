<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
if (!isset($_SESSION["user_manager"])) {
    $_SESSION['header'] = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}/{$_SERVER['REQUEST_URI']}";
    echo " <script>window.location='login.php';</script>";
}
?>

<?php
if (isset($_GET['a'])) {
    # code...
    $username = decrypt($_SESSION["user_manager"]);
    $course_id = decrypt($_GET['a']);

    $modules = "";

    $query = "SELECT courses.id, courses.name, courses.image, courses.description, courses.category, classroom.unique_id, (select count(*) from classroom where classroom.course_id=$course_id and payment_status= 'paid') As students FROM courses join classroom on courses.id = classroom.course_id where classroom.user_id='$username' and courses.id = $course_id and payment_status='paid'";
    $result = $conn->query($query);
    //$number = $result->num_rows;
    if ($result === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $conn->error, E_USER_ERROR);
    } else {
        $result->data_seek(0);
        while ($row = $result->fetch_assoc()) {
            $id = $row["id"];
            $name = $row["name"];
            $category = $row["category"];
            $description = $row["description"];
            $image = $row["image"];
            $students = $row["students"];

            $count = 0;


            $query2 = "SELECT * FROM module where course_id = $id order by `number` asc";
            $result2 = $conn->query($query2);
            $result2->data_seek(0);
            while ($row2 = $result2->fetch_assoc()) {
                $module_name = $row2["name"];
                $module_number = $row2["number"];
                $module_location = $row2["location"];

                $count++;

                $modules .= '
                                            <div class="col-md-3">
                                                <div class="venue-item">         
                                                    <div class="details">
                                                        <h3>Module ' . $module_number . '</h3>
                                                        <h4>' . $module_name . '</h4>
                                                        <a class="btn btn-default" href="courses/course_' . $course_id . '_' . $module_location . '" download>Download</a>
                                                        
                                                        <!-- <div class="venue__instructor">
                                                            <h4 class="instructor__heading">Instructor(s)</h4>
                                                            <div class="instructor__name">Olayemi Chukwudi</div>
                                                            <div class="instructor__name">Olayemi Chukwudi</div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                    ';
            }

        }

    }
    $result->free();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

<div class="remodal-bg">
    <?php include("header.php"); ?>
    <section id="course-hero"
             style="background-image: linear-gradient(-180deg, rgba(0,0,0,0.1) 0%, rgba(0,0,0,0.7) 100%),url(course_icons/<?= $image ?>);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="course-details">
                        <h1 class="course-title"><?= $name ?></h1>
                        <p class="course-desc"><?= $description ?>.</p>

                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="more-details">
                            <div class="price"><i class="ion-person"></i><?= $students . " Student(s)" ?></div>
                            <!-- <div class="location"><i class="ion-ios-location"></i><?= $type; ?></div> -->
                            <!--<div class="duration"><i class="ion-calendar"></i>Sept 12 – Feb 13, 2016.</div>-->
                        </div>
                    </div>
                    <!-- <div class="col-sm-4">
                        <div class="button-wrap">
                            <a href="#enroll-modal" class="button full-width big blue">Enroll for this Course <i class="ion-chevron-right"></i></a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>

    <section id="courses" class="course-page">
        <div class="container">
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>Modules</h2>
                </div>
                <div class="col-md-3">
                    <a class="button disabled"><?= $count ?> Modules</a>
                </div>
            </div>
            <!-- END First Row-->

            <div class="row venues-row">

                <?= $modules; ?>

                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
            <div class="row first-row">
                <div class="col-md-3">
                    <h2>Similar Courses</h2>
                </div>
                <div class="col-md-3">
                    <a href="courses.php" class="button">View All Courses <i class="ion-ios-arrow-right"></i></a>
                    <!-- <a href="#" class="button disabled">4 Courses</a> -->
                </div>
            </div>
            <!-- END First Row-->

            <div class="row courses-row">
                <?php
                $results = $mysqli->prepare("SELECT id, name, image, description FROM courses where status='active' and category='$category' and id != '$course_id' ORDER BY id DESC LIMIT 4");
                $results->execute(); //Execute prepared Query
                $results->bind_result($id, $name, $image, $description); //bind variables to prepared statement

                $description = strip_tags($description);

                if (strlen($description) > 500) {

                    // truncate string
                    $descriptionCut = substr($description, 0, 500);

                    // make sure it ends in a word so assassinate doesn't become ass...
                    $description = substr($descriptionCut, 0, strrpos($descriptionCut, ' ')) . '...';
                }
                //Display records fetched from database.

                while ($results->fetch()) { //fetch values
                    echo '<div class="col-md-3">
                                            <a href="course.php?a=' . encrypt($id) . '">
                                            <div class="course-card">
                                                <div class="image" style="background-image: url(course_icons/' . $image . ')">
                                                    
                                                </div>
                                                <div class="details">
                                                    <h3>' . $name . '</h3>
                                                    <p>' . $description . '</p>
                                                </div>
                                            </div>
                                            </a>
                                        </div>';
                }
                ?>

            </div>
        </div>
    </section>
    <?= $modal; ?>
    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>