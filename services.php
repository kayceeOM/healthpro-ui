<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">

    <?php
    session_start();
    include("header.php"); ?>


    <section id="services">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Apple Green Services</h2>

                <div class="services">

                    <div class="col-sm-4">
                        <div class="service">
                            <h3>Continuous Professional Development</h3>
                            <ul>
                                <li>MCPDP for Nurses</li>
                                <li>CPD for Doctors</li>
                                <li>CPD for Pharmacists</li>
                                <li>CPD for Lab Scientists</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="service">
                            <h3>Recruitment and Staffing</h3>
                            <ul>
                                <li>Recruitment and deployment of Site medics</li>
                                <li>Project Team</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="service">
                            <h3>Training and Development</h3>
                            <ul>
                                <li>Life saver</li>
                                <li>Safety Courses</li>
                                <li>Quality management</li>
                                <li>Productivity courses</li>
                                <li>Customized courses</li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-8">
                        <div class="service">
                            <h3>Healthcare Projects and Advisory Services</h3>

                            <div class="row">
                                <div class="col-sm-5">
                                    <ul>
                                        <li>Industrial / Corporate Clinic management</li>
                                        <li>Health Plan Advisory</li>
                                        <li>Hospital facility Assessment and On-boarding</li>
                                        <li>Community Health Needs Assessment</li>
                                        <li>Health-Based Corporate Social Responsibility Project</li>
                                        <li>Health Business Intelligence</li>
                                        <li>Health Project Management</li>
                                        <li>HMO Brokerage Services</li>
                                        <li>Corporate Health needs Assessment</li>
                                    </ul>
                                </div>

                                <div class="col-sm-7">
                                    <p>Quality is everything in healthcare. Whether you're a corporate body organizing
                                        healthcare projects/CSR for a host community; managing an industrial clinic.</p>

                                    <p>Our innovative and dynamic team offer significant benefits for healthcare and
                                        corporate organizations. We provide hands-on approach allows with customized
                                        solutions to the most challenging situations.</p>

                                    <p>We work with healthcare and corporate organizations through major health
                                        initiatives using creative strategies with proven results. We employ cost-saving
                                        and revenue-enhancing solutions, allowing our healthcare clients to reduce costs
                                        while improving patient outcomes, improve productivity and stimulate growth.</p>

                                    <p>Patients are at the receiving end and deserve the best available form of
                                        care.</p>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="service">
                            <h3>Others</h3>
                            <ul>
                                <li>Health Seminars and workshops</li>
                                <li>Events</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>