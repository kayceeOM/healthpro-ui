<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php
    session_start();
    include("header.php"); ?>

    <section id="payment">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="payment-card">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                <div class="icon fail">
                                    <i class="ion ion-close"></i>
                                </div>

                                <h2>Subscription Failed</h2>
                                <p>Sorry, your subscription was not possible. <br/>Try it again, or contact us if you're
                                    having any issues.</p>

                                <a href="index.php">Retry <i class="ion-refresh"></i></a>
                                <!-- <a href="">Get Help <i class="ion-information-circled"></i></a> -->
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
<!-- <script src="js/main.js"></script> -->

</body>
</html>