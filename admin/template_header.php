<?php

$user = $_SESSION['admin_manager'];
$user = decrypt($user);
$user_type = $_SESSION['admin_type'];
$user_type = decrypt($user_type);
?>
<header class="main-header">

    <!-- Logo -->
    <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b><i class="fa fa-book"></i></b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><i class="fa fa-book"></i> HealthPro</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- Notifications: style can be found in dropdown.less -->


                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="images/avatar.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $user ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="images/avatar.png" class="img-circle" alt="User Image">
                            <p>
                                <?php echo $user ?> - HealthPro

                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="account.php" class="btn btn-default btn-flat">Change Password</a>
                            </div>
                            <div class="pull-right">
                                <a href="signout.php" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>

    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="images/avatar.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $user ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php
        $sidebar = "";
        if ($user_type == "super") {
            $sidebar .= '
                  <ul class="sidebar-menu">
                  <li class="header">MAIN NAVIGATION</li>
                  <li class="active treeview">
                    <a href="index.php">
                      <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                    </a>
                  </li>
                  
                  <li>
                    <a href="all_courses.php">
                <?php
             include_once("../storescripts/connect_to_mysql.php");
              $courses = mysqli_query($conn,"select * from courses") or die(mysqli_error($conn));
              $courseCount = mysqli_affected_rows($conn);
               // count the output amount
              ?>
                      <i class="fa fa-book"></i> <span> View all courses</span> <small class="label pull-right bg-green"><?php echo $courseCount ?></small>
                    </a>
                  </li>
                  <li>
                    <a href="all_categories.php">
                <?php
              $course_categories = mysqli_query($conn,"select * from category") or die(mysqli_error($conn));
              $categoriesCount = mysqli_affected_rows($conn);
               // count the output amount
              ?>
                      <i class="fa fa-tags"></i> <span> View all Categories</span> <small class="label pull-right bg-green"><?php echo $categoriesCount ?></small>
                    </a>
                  </li>
                  <li>
                    <a href="all_coupons.php">
                <?php
              $course_coupons = mysqli_query($conn,"select * from coupon") or die(mysqli_error($conn));
              $couponsCount = mysqli_affected_rows($conn);
               // count the output amount
              ?>
                      <i class="fa fa-ticket"></i> <span> View all Coupons</span> <small class="label pull-right bg-green"><?php echo $couponsCount ?></small>
                    </a>
                  </li>
             <li>
             <li>
                    <a href="all_users.php">
                <?php
              $course_users = mysqli_query($conn,"select * from account") or die(mysqli_error($conn));
              $userCount = mysqli_affected_rows($conn);
               // count the output amount
              ?>
                      <i class="fa fa-users"></i> <span> View all Users</span> <small class="label pull-right bg-green"><?php echo $userCount ?></small>
                    </a>
                  </li>
             <li>
             <li>
                    <a href="all_locations.php">
                <?php
              $course_locations = mysqli_query($conn,"select * from training_loc") or die(mysqli_error($conn));
              $locationCount = mysqli_affected_rows($conn);
               // count the output amount
              ?>
                      <i class="fa fa-map-marker"></i> <span> View all Training Locations</span> <small class="label pull-right bg-green"><?php echo $locationCount ?></small>
                    </a>
                  </li>
                  <li>
                    <a href="subscribers.php">
                      <i class="fa fa-users"></i> <span> All subscribers</span>
                    </a>
                  </li>
                  
                  <li>
                    <a href="all_admin.php">
                      <i class="fa fa-users"></i> <span> All Admin users</span>
                    </a>
                  </li>
                  
                    <li>
                    <a href="new_user.php">
                      <i class="fa fa-user"></i> <span> Add new Admin user</span>
                    </a>
                  </li>
                  
                  </ul>
                  ';
        } elseif ($user_type == "normal") {
            $sidebar .= '

                  <ul class="sidebar-menu">
                  <li class="header">MAIN NAVIGATION</li>
                  <li class="active treeview">
                    <a href="index.php">
                      <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                    </a>
                  </li>
                  
                  <li>
                    <a href="all_courses.php">
                        <?php
                     include_once("../storescripts/connect_to_mysql.php");
                      $courses = mysqli_query($conn,"select * from courses") or die(mysqli_error($conn));
                      $courseCount = mysqli_affected_rows($conn);
                       // count the output amount
                      ?>
                      <i class="fa fa-book"></i> <span>Courses</span> <small class="label pull-right bg-green"><?php echo $courseCount ?></small>
                    </a>
                  </li>
                  <li>
                    <a href="all_students.php">
                        <?php
                     include_once("../storescripts/connect_to_mysql.php");
                      ?>
                      <i class="fa fa-users"></i> <span>My Students</span></small>
                    </a>
                  </li>
                  
                  </ul>
                  ';
        }

        ?>
        <?php echo $sidebar; ?>
    </section>
    <!-- /.sidebar -->
</aside>