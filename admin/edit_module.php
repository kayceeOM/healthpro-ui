<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
$reply = '';
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}
$user = $_SESSION['admin_manager'];
$user = decrypt($user);
$user_type = $_SESSION['admin_type'];
$user_type = decrypt($user_type);
?>

<?php
if (isset($_GET['module'])) {
    $module = decrypt($_GET['module']);
    $module_list = "";
    $modules = mysqli_query($conn, "select * from module where id = '$module'") or die(mysqli_error($conn));
    $modulesCount = mysqli_affected_rows($conn);
    if ($modulesCount > 0) {
        while ($row = mysqli_fetch_array($modules)) {
            $module_id = $row["id"];
            $name = $row["name"];
            $course_id = $row["course_id"];
            $location = $row["location"];
            $number = $row["number"];
            $date_created = $row["date_created"];

        }
    }
}
?>
<?php
if (isset($_POST['updateButton'])) {
    $course_id = $_POST['course_id'];
    $module_id = $_POST['module_id'];
    $name = $_POST['name'];
    $location = $_POST['location'];
    $course_number = $_POST['course_number'];
    $module_item = $_FILES['module_item']['name'];

    if ($module_item == '') {
        $module_item = $location;
    } else {

        $module_item_temp1 = $_FILES['module_item']['tmp_name'];
        move_uploaded_file($module_item_temp1, "../courses/course_" . $course_id . "_" . $module_item . "");

    }


    $updatequery = "update module set course_id = " . $course_id . ", name = '" . $name . "', number = " . $course_number . ", location = '" . $module_item . "' where id = " . $module_id . "";

    //echo  $updatequery;  
    $updateModule = mysqli_query($conn, $updatequery) or die(mysqli_error($conn));
    if ($updateModule) {

        $reply = '
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Module has been updated
    </div>
    ';
    } else {
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Error updating module
    </div>
    ';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Module | <?= $name ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- <script>
    tinymce.init({
      selector: '#mytextarea',
    });
   
    </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Module: <?= $name ?>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"> Module</a></li>
                <li class="active">Edit Module: <?= $name ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                Edit Module
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="edit_module.php?module=<?= encrypt($module_id) ?>"
                                    ">
                                    <?= $reply ?>
                                    <div class="form-group">
                                        <label for="name">Module Name</label>
                                        <input name="module_id" class="form-control" type="hidden" id="module_id"
                                               value="<?= $module_id ?>" / required>
                                        <input name="course_id" class="form-control" type="hidden" id="course_id"
                                               value="<?= $course_id ?>" / required>
                                        <input name="name" class="form-control" type="text" id="name"
                                               value="<?= $name ?>" / required>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Module Number</label>
                                        <input name="course_number" class="form-control" type="number"
                                               pattern='[0-9]{10}' id="course_number" value="<?= $number ?>" / required>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="module_item">Upload Module </label>
                                        <input name="location" type="hidden" id="location" value="<?= $location ?>"/>
                                        <input type="file" name="module_item"/>
                                    </div>
                                    <input type="submit" name="updateButton" id="updateButton" value="Update Module"
                                           class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>

<script src="plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
