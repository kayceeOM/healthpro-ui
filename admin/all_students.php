<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
?>

<?php
// This block grabs the whole list for viewing
$user = $_SESSION['admin_manager'];
$user = decrypt($user);
//echo $user;
$user_list = "";
$shop_products = mysqli_query($conn, "SELECT account.firstname, account.lastname, account.email, account.phone, courses.name FROM `account` join classroom on classroom.user_id = account.email join courses on classroom.course_id = courses.id WHERE classroom.course_id IN (SELECT id from courses where uploader = '$user') and classroom.payment_status = 'paid' GROUP BY courses.name ORDER BY courses.name ASC") or die(mysqli_error($conn));
$productCount = mysqli_affected_rows($conn);
$status = '';
if ($productCount > 0) {
    while ($row = mysqli_fetch_array($shop_products)) {
        $fname = $row["firstname"];
        $lname = $row["lastname"];
        $username = $fname . " " . $lname;
        $email = $row["email"];
        $phone = $row["phone"];
        $course = $row["name"];


        $user_list .= " 
 
				<tr>
					<td>$username</td>
					<td>$email</td> 
					<td>$phone</td>
                    <td>$course</td>
				  </tr>

			 ";

    }
} else {
    //$user_list = "You have no products listed in your store yet";
}
?>
<?php
if (isset($_POST["export"])) {
    include('simple_html_dom.php');
    $header = '<tr>
                        <td>User Name</td>
                        <td>Email</td> 
                        <td>Phone</td>
                        <td>Course</td>
                    </tr>';
    $html = str_get_html($header . "" . $user_list); // give this your HTML string

    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=users.csv');

    $fp = fopen("php://output", "w");

    foreach ($html->find('tr') as $element) {
        $td = array();
        foreach ($element->find('th') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }

        $td = array();
        foreach ($element->find('td') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }
    }

    fclose($fp);
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Learning | All Students</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Students

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Users</a></li>
                <li class="active">All Users</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <div class="table-responsive">
                            <div class="box-body">

                                <div class="col-xs-3 pull-left">
                                    <form name="exportform" method="post" action="all_users.php">
                                        <input type="submit" name="export" Value="Export Form"
                                               class="btn btn-sm btn-default btn-flat"/>
                                    </form>
                                </div>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Course</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $user_list; ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.box -->

                </div><!-- /.col -->

            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>
</body>
</html>
