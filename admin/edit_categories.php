<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
?>

<?php
if (isset($_POST['updateButton'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];

    $updatequery = "Update category set name = '" . $name . "' where id=" . $id . "";
    $updatecategory = mysqli_query($conn, $updatequery) or die(mysqli_error($conn));
    if ($updatecategory) {
        echo " <script>alert('Category has been Edited');</script>";
        echo " <script>window.location='all_categories.php';</script>";
    } else {
        echo " <script>alert('Error! Category not Edited');</script>";
    }
}

?>

<?php
// This block grabs the whole list for viewing
if (isset($_GET["pid"])) {
    $category = $_GET["pid"];
    $shop_category = mysqli_query($conn, "select * from category where id=" . $category . "") or die(mysqli_error($conn));
    $productCount = mysqli_affected_rows($conn);
    if ($productCount > 0) {
        while ($row = mysqli_fetch_array($shop_category)) {
            $id = $row["id"];
            $name = $row["name"];
        }
    }
} else {
    //$course_list = "You have no courses listed in your store yet";
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HealthPro | Edit Category</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <!--   <script>
      tinymce.init({
        selector: '#mytextarea',
      });
     
      </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Category - <?= $name ?>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Categories</a></li>
                <li class="active">Edit Categories</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                Edit Category
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="edit_categories.php">
                                        <div class="form-group">
                                            <label for="name">Category Name</label>
                                            <input name="id" type="hidden" value="<?= $id ?>" / required>
                                            <input name="name" class="form-control" type="text" id="name"
                                                   value="<?= $name ?>" / required>
                                        </div>

                                        <input type="submit" name="updateButton" id="updateButton"
                                               value="Update Category"
                                               class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
