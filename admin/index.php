<?php
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
//connecting to database
?>
<?php
// This block grabs the whole list for viewing
$user = $_SESSION['admin_manager'];
$user = decrypt($user);
$user_type = $_SESSION['admin_type'];
$user_type = decrypt($user_type);
$course_list = "";
$sum = '';
$account_sum = '';
$user_couser_count = mysqli_query($conn, "select * from courses where uploader = '$user'") or die(mysqli_error($conn));
$myCourseCount = mysqli_affected_rows($conn);

$user_student_count = mysqli_query($conn, "SELECT account.firstname, account.lastname, account.email, account.phone, courses.name FROM `account` join classroom on classroom.user_id = account.email join courses on classroom.course_id = courses.id WHERE classroom.course_id IN (SELECT id from courses where uploader = '$user') and classroom.payment_status = 'paid' GROUP BY courses.name ORDER BY courses.name ASC") or die(mysqli_error($conn));
$myStudentCount = mysqli_affected_rows($conn);

$course_accounts = mysqli_query($conn, "select * from account") or die(mysqli_error($conn));
$account_sum = mysqli_affected_rows($conn);

$admin_accounts = mysqli_query($conn, "select * from admin where user_type='normal'") or die(mysqli_error($conn));
$admin_sum = mysqli_affected_rows($conn);

$category_accounts = mysqli_query($conn, "select * from category") or die(mysqli_error($conn));
$category_sum = mysqli_affected_rows($conn);

switch ($user_type) {
    case "super":
        $courses = mysqli_query($conn, "select * from courses limit 10") or die(mysqli_error($conn));
        $courseCount = mysqli_affected_rows($conn);
        if ($courseCount > 0) {
            while ($row = mysqli_fetch_array($courses)) {
                $id = $row["id"];
                $name = $row["name"];
                $image = $row["image"];
                $type = $row["type"];
                $cost = $row["cost"];
                $date_added = strftime("%b %d, %Y", strtotime($row["date_created"]));

                $course_list .= "
                <tr>
                  <td>
                      <div class='product-img'>
                      <img height='30px' width='30px' src='../course_icons/" . $image . "' alt='Product Image'>
                      </div>
                  </td> 
                  <td>$name</td>
                  <td>$type</td> 
                  <td>$cost</td> 
                  <td>$date_added</td> 
                  <td><a class='tiny button' href='edit_courses.php?pid=$id'>edit</a></td>
                  <td><a class='tiny button' href='all_courses.php?deleteid=$id'>delete</a></td>
                  
                  </tr>

               ";
            }
        } else {
            //$course_list = "You have no products in our database yet";
        }


        $shop_products = mysqli_query($conn, "select * from account group by email limit 10") or die(mysqli_error($conn));
        $productCount = mysqli_affected_rows($conn);
        $status = '';
        $user_list = '';
        if ($productCount > 0) {
            while ($row = mysqli_fetch_array($shop_products)) {
                $firstname = $row["firstname"];
                $lastname = $row["lastname"];
                $name = $firstname . " " . $lastname;
                $email = $row["email"];
                $phone = $row["phone"];
                $credit_points = $row["credit_points"];

                $user_list .= " 
 
				<tr>
					<td>$name</td>
					<td>$email</td> 
					<td>$phone</td>
				  </tr>

			 ";

            }
        } else {
            //$user_list = "You have no products listed in your store yet";
        }
        break;
    case "normal":
        $courses = mysqli_query($conn, "select * from courses where uploader = '$user' limit 10") or die(mysqli_error($conn));
        $courseCount = mysqli_affected_rows($conn);
        if ($courseCount > 0) {
            while ($row = mysqli_fetch_array($courses)) {
                $id = $row["id"];
                $name = $row["name"];
                $image = $row["image"];
                $type = $row["type"];
                $cost = $row["cost"];
                $date_added = strftime("%b %d, %Y", strtotime($row["date_created"]));

                $course_list .= "
                <tr>
                  <td>
                      <div class='product-img'>
                      <img height='30px' width='30px' src='../course_icons/" . $image . "' alt='Product Image'>
                      </div>
                  </td> 
                  <td>$name</td>
                  <td>$type</td> 
                  <td>$cost</td> 
                  <td>$date_added</td> 
                  <td><a class='tiny button' href='edit_courses.php?pid=$id'>edit</a></td>
                  <td><a class='tiny button' href='all_courses.php?deleteid=$id'>delete</a></td>
                  
                  </tr>

               ";
            }
        } else {
            //$course_list = "You have no products in our database yet";
        }

        $user = $_SESSION['admin_manager'];
        $user = decrypt($user);
//echo $user;
        $user_list = "";
        $shop_products = mysqli_query($conn, "SELECT account.firstname, account.lastname, account.email, account.phone, courses.name FROM `account` join classroom on classroom.user_id = account.email join courses on classroom.course_id = courses.id WHERE classroom.course_id IN (SELECT id from courses where uploader = '$user') and classroom.payment_status = 'paid' GROUP BY courses.name ORDER BY courses.name ASC") or die(mysqli_error($conn));
        $productCount = mysqli_affected_rows($conn);
        $status = '';
        if ($productCount > 0) {
            while ($row = mysqli_fetch_array($shop_products)) {
                $fname = $row["firstname"];
                $lname = $row["lastname"];
                $username = $fname . " " . $lname;
                $email = $row["email"];
                $phone = $row["phone"];
                $course = $row["name"];


                $user_list .= " 
 
				<tr>
					<td>$username</td>
					<td>$email</td> 
					<td>$phone</td>
                    
				  </tr>

			 ";

            }
        } else {
            //$user_list = "You have no products listed in your store yet";
        }

        break;
    default:
        //$course_list = "You have no products in our database yet";
}


?>
<?php
// This block grabs the whole list for viewing

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Learning Portal | Admin Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                HealthPro Dashboard
                <small>Version 1.0</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <?php
            $headbar = '';
            if ($user_type == "super") {
                $headbar .= '
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">All Courses</span>
                            <span class="info-box-number">' . $courseCount . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col -->
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">All Users</span>
                            <span class="info-box-number">' . $account_sum . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col -->
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">All Admin</span>
                            <span class="info-box-number">' . $admin_sum . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col --><div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-red"><i class="fa fa-cog"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">All Categories</span>
                            <span class="info-box-number">' . $category_sum . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col -->
                      <!-- fix for small devices only -->
                      <div class="clearfix visible-sm-block"></div>

                     
                    </div><!-- /.row -->
                 ';
            } elseif ($user_type == "normal") {
                $headbar .= '
                    <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">All Courses</span>
                            <span class="info-box-number">' . $myCourseCount . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col -->
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info-box">
                          <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">My Students</span>
                            <span class="info-box-number">' . $myStudentCount . '</span>
                          </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                      </div><!-- /.col -->
                      <!-- fix for small devices only -->
                      <div class="clearfix visible-sm-block"></div>

                     
                    </div><!-- /.row -->
                 ';
            }
            ?>
            <?php echo $headbar ?>
            <!-- Main row -->
            <div class="row">
                <div class="col-md-12">

                    <!-- course LIST -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recently Added courses</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">

                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Course Name</th>
                                        <th>Course Type</th>
                                        <th>Cost</th>
                                        <th>Date Added</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $course_list; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="all_courses.php" class="uppercase">View All courses</a>
                        </div><!-- /.box-footer -->
                    </div><!-- /.box -->

                    <!-- USER LIST -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">New Users</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">

                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $user_list; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box-body -->
                        <div class="box-footer text-center">
                            <?php

                            if ($user_type == "super") {
                                echo '<a href = "all_users.php" class="uppercase" > View All Users </a >';
                            } elseif ($user_type == "normal") {
                                echo '<a href = "all_students.php" class="uppercase" > View All Students </a >';
                            }
                            ?>
                        </div><!-- /.box-footer -->
                    </div><!-- /.box -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <?php include_once("footer.php") ?>
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
