<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}
$user = $_SESSION['admin_manager'];
$user = decrypt($user);
$user_type = $_SESSION['admin_type'];
$user_type = decrypt($user_type);
?>

<?php

//activate a course
if (isset($_GET['aid'])) {
    $aid = $_GET['aid'];
    $sql = mysqli_query($conn, "update courses set status = 'active' where id=" . $aid . "") or die (mysqli_error($conn));
    echo " <script>window.location='all_courses.php';</script>";

}
//Deactivate a course
if (isset($_GET['cid'])) {
    $cid = $_GET['cid'];
    $sql = mysqli_query($conn, "update courses set status = 'pending' where id=" . $cid . "") or die (mysqli_error($conn));
    echo " <script>window.location='all_courses.php';</script>";
}
// Delete Item Question to Admin, and Delete Product if they choose
if (isset($_GET['deleteid'])) {
    echo "<script>
         var r = confirm('delete course?');
          if (r == true) {
              window.location='all_courses.php?yesdelete=" . $_GET['deleteid'] . "';
          } else {
              window.location='all_courses.php';
          }
 </script>";
    // echo 'Do you really want to delete Course with ID of ' . $_GET['deleteid'] . '? <a href="' . $_GET['deleteid'] . '">Yes</a> | <a href="all_courses.php">No</a>';
    exit();
}
if (isset($_GET['yesdelete'])) {
    // remove item from system and delete its picture
    // delete from database
    $id_to_delete = $_GET['yesdelete'];
    $sql = mysqli_query($conn, "DELETE FROM courses WHERE id='$id_to_delete' LIMIT 1") or die (mysqli_error($conn));
    // unlink the image from server
    // Remove The Pic -------------------------------------------
    $shop_Courses = mysqli_query($conn, "select image from courses where id = '$id_to_delete' ") or die(mysqli_error($conn));
    $CourseCount = mysqli_affected_rows($conn);
    if ($CourseCount > 0) {
        while ($row = mysqli_fetch_array($shop_Courses)) {
            $image = $row[0];
            $pictodelete = ("../course_icons/" . $image . "");
            if (file_exists($pictodelete)) {
                unlink($pictodelete);
            }
        }
        echo " <script>window.location='all_courses.php';</script>";
    }
}
?>

<?php
if (isset($_POST['insertButton'])) {
    $name = $_POST['name'];
    $type = $_POST['type'];
    $category = $_POST['category'];
    $image = $_FILES['image']['name'];
    $cost = $_POST['cost'];
    $train_loc = $_POST['train_loc'];
    $description = $_POST['description'];
    $speciality = $_POST['speciality'];
    $prof_point = $_POST['prof_point'];
    if ($prof_point == '') {
        # code...
        $prof_point = 'Null';
    }
    // convert training locations to json array
    $train_loc = json_encode($train_loc);

    $insertquery = "Insert into Courses (name, type, training_loc, category, image, cost, description,
   uploader, prof_point, speciality, rating, raters, date_created, status)
    values ('" . $name . "', '" . $type . "', '" . $train_loc . "', '" . $category . "', '" . $image . "', '" . $cost . "',
     '" . $description . "','" . decrypt($_SESSION["admin_manager"]) . "', " . $prof_point . ",
      '" . $speciality . "', '0', '0',  now(), 'pending')";
    //echo  $insertquery;  
    $insertCourse = mysqli_query($conn, $insertquery) or die(mysqli_error($conn));
    if ($insertCourse) {
        //$image = $_FILES['image']['name'];
        $image_temp1 = $_FILES['image']['tmp_name'];
        move_uploaded_file($image_temp1, "../course_icons/$image");

        echo " <script>alert('Course has been added');</script>";
        echo " <script>window.location='all_courses.php';</script>";
    } else {
        echo " <script>alert('Error! Course not added');</script>";
    }
}

?>


<?php
$Course_list = "";
switch ($user_type) {
    case "super":
        $courses = mysqli_query($conn, "select * from courses") or die(mysqli_error($conn));
        $courseCount = mysqli_affected_rows($conn);
        if ($courseCount > 0) {
            while ($row = mysqli_fetch_array($courses)) {
                $id = $row["id"];
                $name = $row["name"];
                $image = $row["image"];
                $type = $row["type"];
                $cost = $row["cost"];
                $category = $row["category"];
                $prof_point = $row["prof_point"];
                $uploader = $row["uploader"];
                $status = $row["status"];
                $date_added = strftime("%b %d, %Y", strtotime($row["date_created"]));

                $course_modules = mysqli_query($conn, "select * from module where course_id = '$id'") or die(mysqli_error($conn));
                $modules_sum = mysqli_affected_rows($conn);

                if ($status != 'active') {
                    $Course_list .= " 
 
                <tr>
                  <td>
                      <div class='product-img'>
                      <img height='30px' width='30px' src='../course_icons/" . $image . "' alt='Product Image'>
                      </div>
                  </td> 
                  <td>$name</td>
                  <td>$type</td> 
                  <td>$cost</td> 
                  <td>$category</td>
                  <td>$prof_point</td>
                  <td>$modules_sum</td>  
                  <td>$uploader</td> 
                  <td>$status</td> 
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='modules.php?course=" . encrypt($id) . "' style='margin:0 auto;' class='btn btn-primary'>
                        Modules
                      </a>
                    </div>
                    </td>
                    <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='all_courses.php?aid=$id' style='margin:0 auto;' class='btn btn-primary'>
                        Activate
                      </a>
                    </div>
                    </td>
                  <td align='center'>
                    <div class='btn-group'>
                      <a href='edit_courses.php?pid=$id' style='margin:0 auto;' class='btn btn-primary text-center'>
                        <span class='fa fa-cog'></span>
                      </a>
                    </div>
                  </td>
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='all_courses.php?deleteid=$id' style='margin:0 auto;' class='btn btn-primary'>
                        <span class='fa fa-trash'></span>
                      </a>
                    </div>
                    </td>
                  </tr>

               ";
                } else {
                    $Course_list .= " 
 
                <tr>
                  <td>
                      <div class='product-img'>
                      <img height='30px' width='30px' src='../course_icons/" . $image . "' alt='Product Image'>
                      </div>
                  </td> 
                  <td>$name</td>
                  <td>$type</td> 
                  <td>$cost</td> 
                  <td>$category</td>
                  <td>$prof_point</td> 
                  <td>$modules_sum</td> 
                  <td>$uploader</td> 
                  <td>$status</td> 
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='modules.php?course=" . encrypt($id) . "' style='margin:0 auto;' class='btn btn-primary'>
                        Modules
                      </a>
                    </div>
                    </td>
                    <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='all_courses.php?cid=$id' style='margin:0 auto;' class='btn btn-primary'>
                        Deactivate
                      </a>
                    </div>
                    </td>

                  <td align='center'>
                    <div class='btn-group'>
                      <a href='edit_courses.php?pid=$id' style='margin:0 auto;' class='btn btn-primary text-center'>
                        <span class='fa fa-cog'></span>
                      </a>
                    </div>
                  </td>
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='all_courses.php?deleteid=$id' style='margin:0 auto;' class='btn btn-primary'>
                        <span class='fa fa-trash'></span>
                      </a>
                    </div>
                    </td>
                  </tr>

               ";
                }
            }
        } else {
            //$course_list = "You have no products in our database yet";
        }
        break;
    case "normal":
        $courses = mysqli_query($conn, "select * from courses where uploader = '$user'") or die(mysqli_error($conn));
        $courseCount = mysqli_affected_rows($conn);
        if ($courseCount > 0) {
            while ($row = mysqli_fetch_array($courses)) {
                $id = $row["id"];
                $name = $row["name"];
                $image = $row["image"];
                $type = $row["type"];
                $cost = $row["cost"];
                $category = $row["category"];
                $prof_point = $row["prof_point"];
                $uploader = $row["uploader"];
                $status = $row["status"];
                $date_added = strftime("%b %d, %Y", strtotime($row["date_created"]));

                $course_modules = mysqli_query($conn, "select * from module where course_id = '$id'") or die(mysqli_error($conn));
                $modules_sum = mysqli_affected_rows($conn);

                $Course_list .= " 
                <tr>
                  <td>
                      <div class='product-img'>
                      <img height='30px' width='30px' src='../course_icons/" . $image . "' alt='Product Image'>
                      </div>
                  </td> 
                  <td>$name</td>
                  <td>$type</td> 
                  <td>$cost</td> 
                  <td>$category</td>
                  <td>$prof_point</td>
                  <td>$modules_sum</td>
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='modules.php?course=$id' style='margin:0 auto;' class='btn btn-primary'>
                        Modules
                      </a>
                    </div>
                    </td>
                  <td align='center'>
                    <div class='btn-group'>
                      <a href='edit_courses.php?pid=$id' style='margin:0 auto;' class='btn btn-primary text-center'>
                        <span class='fa fa-cog'></span>
                      </a>
                    </div>
                  </td>
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='all_courses.php?deleteid=$id' style='margin:0 auto;' class='btn btn-primary'>
                        <span class='fa fa-trash'></span>
                      </a>
                    </div>
                    </td>
                  </tr>

               ";
            }
        } else {
            //$course_list = "You have no products in our database yet";
        }
        break;
    default:
        //$course_list = "You have no products in our database yet";
}
?>

<?php
if (isset($_POST["export"])) {
    include('simple_html_dom.php');
    $header = '<tr>
                        <th>ID</th>
                        <th></th>
                        <th>Course Name</th>
                        <th>Course Type</th>
                        <th>Cost</th>   
                        <th>Date Added</th>
                        <th></th>
                        <th></th>
                      </tr>';
    $html = str_get_html($header . "" . $Course_list); // give this your HTML string

    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=courses.csv');

    $fp = fopen("php://output", "w");

    foreach ($html->find('tr') as $element) {
        $td = array();
        foreach ($element->find('th') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }

        $td = array();
        foreach ($element->find('td') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }
    }

    fclose($fp);
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Courses | All Courses</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- <script>
    tinymce.init({
      selector: '#mytextarea',
    });
   
    </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Courses

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Courses</a></li>
                <li class="active">All Courses</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div class="col-xs-3 pull-left">
                                    <form name="exportform" method="post" action="all_courses.php">
                                        <input type="submit" name="export" Value="Export Form"
                                               class="btn btn-sm btn-default btn-flat"/>
                                    </form>
                                </div>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Course Name</th>
                                        <th>Course Type</th>
                                        <th>Cost</th>
                                        <th>Category</th>
                                        <th>Professional points</th>
                                        <th>Modules</th>
                                        <?php
                                        if ($user_type == 'super') {
                                            echo '
                                  <th>Uploaded by</th>
                                  <th>Status</th>
                                  <th></th>';
                                        }
                                        ?>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $Course_list; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.box -->

                </div><!-- /.col -->

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                New Courses
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="all_courses.php">
                                        <div class="form-group">
                                            <label for="name">Course Name</label>
                                            <input name="name" class="form-control" type="text" id="name"
                                                   placeholder="Course Name" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="type">Course Type</label>
                                            <select class="form-control" name="type" required="">
                                                <option value="online">Online</option>
                                                <option value="classroom">Classroom</option>
                                                <option value="both">Both</option>
                                            </select>
                                        </div>
                                        <div id="trainID" class="form-group">
                                            <label for="train_loc">Training Location</label>
                                            <select name="train_loc[]" class="form-control  select2" id="train_loc[]"
                                                    multiple="multiple" tabindex="1" required="">
                                                <?php
                                                $location_query = mysqli_query($conn, "select * from training_loc");
                                                $location_count = mysqli_affected_rows($conn);
                                                if ($location_count > 1) {
                                                    while ($location_row = mysqli_fetch_array($location_query)) {
                                                        $location_id = $location_row["id"];
                                                        $location_name = $location_row["name"];
                                                        $location_address = $location_row["location"];
                                                        echo "<option value='$location_id||$location_address'>$location_name</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Upload Icon </label>
                                            <input type="file" name="image" accept="image/*" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="category">Course Category</label>
                                            <select class="form-control select2" name="category" id="category"
                                                    required="">
                                                <?php
                                                $category_query = mysqli_query($conn, "select * from category");
                                                $category_count = mysqli_affected_rows($conn);
                                                if ($category_count > 1) {
                                                    while ($course_row = mysqli_fetch_array($category_query)) {
                                                        $category_id = $course_row["id"];
                                                        $category_name = $course_row["name"];
                                                        echo "<option value='$category_name'>$category_name</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="prof_point">Professional Points (Optional)</label>
                                            <input name="prof_point" class="form-control" type="text" id="prof_point"
                                                   placeholder="Professional Points (Optional)">
                                        </div>
                                        <div class="form-group">
                                            <label for="speciality">Speciality</label>
                                            <input name="speciality" class="form-control" type="text" id="speciality"
                                                   placeholder="Speciality" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="cost">Course Cost</label>
                                            <input name="cost" class="form-control" type="text" id="cost"
                                                   placeholder="Course Cost" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Course Description</label>
                                            <textarea name="description" class="form-control" type="text"
                                                      id="mytextarea" / required></textarea>
                                        </div>
                                        <input type="submit" name="insertButton" id="insertButton" value="Insert Course"
                                               class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>

<script src="plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script>
    $(function () {
        $(".select2").select2();
    });
</script>
</body>
</html>
