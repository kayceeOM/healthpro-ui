<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
?>

<?php
$reply = '';
if (isset($_POST['editacc'])) {
    $user = decrypt($_SESSION['admin_manager']);
    $user_type = decrypt($_SESSION['admin_type']);
    $pass = $_POST['pass'];
    $pass2 = $_POST['pass2'];
    if ($pass == $pass2) {
        $password = md5($pass);
        $insertCard = mysqli_query($conn, "update admin set password = '$password' where username='$user' and user_type = '$user_type'");
        if ($insertCard) {
            $reply = '
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Account has been edited
                </div>
                ';
        } else {
            $reply = '
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Error! Account not edited
                </div>
                ';
        }
    } else {
        $reply = '
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Error! Passwords do not match
                </div>
                ';
    }
}
?>
<?php
// This block grabs the whole list for viewing
$user = $_SESSION['admin_manager'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HealthPro | Change Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Change Password

            </h1>
            <ol class="breadcrumb">
                <li><a><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a>Account</a></li>
                <li class="active">Change Password</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="account.php">
                                        <?php echo $reply ?>
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input name="username" class="form-control" type="text" id="username"
                                                   readonly="true" Value="<?php echo $user ?>" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="cardval">Password</label>
                                            <input name="pass" class="form-control" type="password" id="pass"
                                                   placeholder="Password" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="pass2">Repeat Password</label>
                                            <input name="pass2" class="form-control" type="password" id="pass2"
                                                   placeholder="Repeat Password" / required>
                                        </div>
                                        <input type="submit" name="editacc" id="editacc" value="Edit Account"
                                               class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
