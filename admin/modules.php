<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
$reply = '';
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}
$user = $_SESSION['admin_manager'];
$user = decrypt($user);
$user_type = $_SESSION['admin_type'];
$user_type = decrypt($user_type);
?>

<?php

if (isset($_GET['deleteid']) && isset($_GET['course'])) {
    $course = $_GET['course'];
    echo "<script>
         var r = confirm('delete module?');
          if (r == true) {
              window.location='modules.php?yesdelete=" . $_GET['deleteid'] . "&course=$course';
          } else {
              window.location='modules.php?course=$course';
          }
 </script>";
    // echo 'Do you really want to delete Course with ID of ' . $_GET['deleteid'] . '? <a href="' . $_GET['deleteid'] . '">Yes</a> | <a href="all_courses.php">No</a>';
    exit();
}
if (isset($_GET['yesdelete']) && isset($_GET['course'])) {
    // remove item from system and delete its picture
    // delete from database
    $course = decrypt($_GET['course']);
    $id_to_delete = decrypt($_GET['yesdelete']);
    // unlink the image from server
    // Remove The Pic -------------------------------------------
    $modules = mysqli_query($conn, "select location, course_id from module where id = $id_to_delete") or die(mysqli_error($conn));
    $modulesCount = mysqli_affected_rows($conn);
    if ($modulesCount > 0) {
        while ($row = mysqli_fetch_array($modules)) {
            $module = $row[0];
            $course_id = $row[1];
            $moduletodelete = ("../courses/course_" . $course_id . "_" . $module . "");
            if (file_exists($moduletodelete)) {
                unlink($moduletodelete);
            }
        }

        $sql = mysqli_query($conn, "DELETE FROM module WHERE id=$id_to_delete LIMIT 1") or die (mysqli_error($conn));
        echo "<script>window.location='modules.php?course=" . encrypt($course) . "';</script>";
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Module has been Deleted
    </div>
    ';

    }

}
?>

<?php
if (isset($_POST['insertButton'])) {
    $course_id = $_POST['course_id'];
    $name = $_POST['name'];
    $course_number = $_POST['course_number'];
    $module_item = $_FILES['module_item']['name'];

    $insertquery = "Insert into module (course_id, name, number, location, date_created)
    values (" . $course_id . ", '" . $name . "', " . $course_number . ", '" . $module_item . "', now())";
    //echo  $insertquery;  
    $insertCourse = mysqli_query($conn, $insertquery) or die(mysqli_error($conn));
    if ($insertCourse) {
        //$image = $_FILES['image']['name'];
        $module_item_temp1 = $_FILES['module_item']['tmp_name'];
        move_uploaded_file($module_item_temp1, "../courses/course_" . $course_id . "_" . $module_item . "");

        $reply = '
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Module has been added
    </div>
    ';
    } else {
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Error adding module
    </div>
    ';
    }
}

?>


<?php
if (isset($_GET['course'])) {
    $course = decrypt($_GET['course']);
    $query5 = "SELECT * FROM courses where id = '$course'";
    $result5 = $conn->query($query5);
    $result5->data_seek(0);
    while ($row5 = $result5->fetch_assoc()) {
        $course_name = $row5["name"];
    }

    $module_list = "";
    $modules = mysqli_query($conn, "select * from module where course_id = '$course' order by number desc") or die(mysqli_error($conn));
    $modulesCount = mysqli_affected_rows($conn);
    if ($modulesCount > 0) {
        while ($row = mysqli_fetch_array($modules)) {
            $module_id = $row["id"];
            $name = $row["name"];
            $number = $row["number"];
            $date_created = $row["date_created"];

            $module_list .= " 
 
                <tr>
                  </td> 
                  <td>$name</td>
                  <td>$number</td> 
                  <td>$date_created</td> 
                  <td align='center'>
                    <div class='btn-group'>
                      <a href='edit_module.php?module=" . encrypt($module_id) . "' style='margin:0 auto;' class='btn btn-primary text-center'>
                        <span class='fa fa-cog'></span>
                      </a>
                    </div>
                  </td>
                  <td align='center'>
                    <div class='btn-group text-center'>
                      <a href='modules.php?course=" . encrypt($course) . "&deleteid=" . encrypt($module_id) . "' style='margin:0 auto;' class='btn btn-primary'>
                        <span class='fa fa-trash'></span>
                      </a>
                    </div>
                    </td>
                  </tr>

               ";

        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Modules | <?= $course_name ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- <script>
    tinymce.init({
      selector: '#mytextarea',
    });
   
    </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $course_name ?> Modules

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#"> Module</a></li>
                <li class="active"><?= $course_name ?> Modules</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <?php echo $reply ?>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Module Name</th>
                                        <th>Module Type</th>
                                        <th>Added At</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $module_list; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.box -->

                </div><!-- /.col -->

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                New Module
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="modules.php?course=<?= encrypt($course) ?>"
                                    ">
                                    <div class="form-group">
                                        <label for="name">Module Name</label>
                                        <input name="course_id" class="form-control" type="hidden" id="course_id"
                                               value="<?= $course ?>" / required>
                                        <input name="name" class="form-control" type="text" id="name"
                                               placeholder="Course Name" / required>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Module Number</label>
                                        <input name="course_number" class="form-control" type="number"
                                               pattern='[0-9]{10}' id="course_number" placeholder="Course Number" /
                                        required>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="module_item">Upload Module </label>
                                        <input type="file" name="module_item" accept="pdf/*" required/>
                                    </div>
                                    <input type="submit" name="insertButton" id="insertButton" value="Insert Module"
                                           class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>

<script src="plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script>
    $(function () {
        $(".select2").select2();
    });
</script>
</body>
</html>
