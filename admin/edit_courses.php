<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
?>

<?php
if (isset($_POST['updateButton'])) {
    $id = $_POST['id'];
    $location = '';
    $name = $_POST['name'];
    $type = $_POST['type'];
    $train_loc = $_POST['train_loc'];
    $category = $_POST['category'];
    $image = $_FILES['image']['name'];
    $cost = $_POST['cost'];
    $description = $_POST['description'];
    $edited_by = decrypt($_SESSION["admin_manager"]);
    $course_module = $_POST['course_module'];
    $prof_point = $_POST['prof_point'];
    $speciality = $_POST['speciality'];
    $imageicon = $_POST['imageicon'];
    $filename = $_FILES["courseFile"]["name"];
    $uploadcoursefile = $_POST['uploadcoursefile'];

    if ($prof_point == '') {
        # code...
        $prof_point = 'Null';
    }
    // convert training locations to json array
    $train_loc = json_encode($train_loc);

    if ($image == '') {
        $image = $imageicon;
    } else {
        //$image = $_FILES['image']['name'];
        $image_temp1 = $_FILES['image']['tmp_name'];
        move_uploaded_file($image_temp1, "../course_icons/$image");
    }
    $updatequery = "Update courses set name = '" . $name . "', type = '" . $type . "', training_loc = '" . $train_loc . "', category = '" . $category . "',  image = '" . $image . "', cost = '" . $cost . "', description = '" . $description . "', edited_by = '" . $edited_by . "',prof_point = '" . $prof_point . "', speciality = '" . $speciality . "', date_edited = now() where id=" . $id . "";
    $updatecourse = mysqli_query($conn, $updatequery) or die(mysqli_error($conn));
    if ($updatecourse) {
        echo " <script>alert('Course has been Edited');</script>";
        echo " <script>window.location='all_courses.php';</script>";
    } else {
        echo " <script>alert('Error! Course not Edited');</script>";
    }
}

?>

<?php
// This block grabs the whole list for viewing
if (isset($_GET["pid"])) {
    $course = $_GET["pid"];
    $shop_courses = mysqli_query($conn, "select * from courses where id=" . $course . "") or die(mysqli_error($conn));
    $productCount = mysqli_affected_rows($conn);
    if ($productCount > 0) {
        while ($row = mysqli_fetch_array($shop_courses)) {
            $id = $row["id"];
            $name = $row["name"];
            $category = $row['category'];
            $image = $row["image"];
            $type = $row["type"];
            $train_loc = $row['training_loc'];
            $speciality = $row['speciality'];
            $location = $row["location"];
            $description = $row["description"];
            $cost = $row["cost"];
            $course_module = $row['course_module'];
            $prof_point = $row['prof_point'];
        }
    }
} else {
    //$course_list = "You have no courses listed in your store yet";
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>courser | Edit course</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <!--   <script>
      tinymce.init({
        selector: '#mytextarea',
      });
     
      </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit course - <?= $name ?>

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">courses</a></li>
                <li class="active">Edit courses</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                Edit Course
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="edit_courses.php">
                                        <div class="form-group">
                                            <label for="name">Course Name</label>
                                            <input name="id" type="hidden" value="<?= $id ?>" / required>
                                            <input name="name" class="form-control" type="text" id="name"
                                                   value="<?= $name ?>" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="type">Course Type</label>
                                            <select class="form-control" name="type" required="">
                                                <option <?php if ($type == 'online') {
                                                    echo "selected";
                                                } ?> value="online">Online
                                                </option>
                                                <option <?php if ($type == 'classroom') {
                                                    echo "selected";
                                                } ?> value="classroom">Classroom
                                                </option>
                                                <option <?php if ($type == 'both') {
                                                    echo "selected";
                                                } ?> value="both">Both
                                                </option>
                                            </select>
                                        </div>
                                        <div id="trainID" class="form-group">
                                            <label for="train_loc">Training Location</label>
                                            <select name="train_loc[]" class="form-control select2" id="train_loc[]"
                                                    multiple="multiple" tabindex="1" required="">
                                                <?php
                                                $train_locs = json_decode($train_loc, true);
                                                $array = [];
                                                $location_query = mysqli_query($conn, "select * from training_loc");
                                                $location_count = mysqli_affected_rows($conn);
                                                if ($location_count > 1) {
                                                    while ($location_row = mysqli_fetch_array($location_query)) {
                                                        $location_id = $location_row["id"];
                                                        $location_name = $location_row["name"];
                                                        $location_address = $location_row["location"];
                                                        foreach ($train_locs as $key) {
                                                            if ($key == $location_id . "||" . $location_address) {
                                                                array_push($array, "<option selected value='$location_id||$location_address'>$location_name</option>");
                                                            } else {
                                                                array_push($array, "<option value='$location_id||$location_address'>$location_name</option>");
                                                            }
                                                        }
                                                        array_push($array, "<option value='$location_id||$location_address'>$location_name</option>");
                                                    }
                                                    $result = array_unique($array);
                                                    foreach ($result as $key1) {
                                                        echo $key1;
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image">Upload Icon </label>
                                            <input type="hidden" name="imageicon" value="<?= $image ?>" required/>
                                            <input type="file" name="image" accept="image/*" value="<?= $image ?> />
				  </div>
          <div class=" form-group">
                                            <label for="category">Course Category</label>
                                            <select class="form-control select2" name="category" id="category"
                                                    required="">
                                                <?php
                                                $category_query = mysqli_query($conn, "select * from category");
                                                $category_count = mysqli_affected_rows($conn);
                                                if ($category_count > 1) {
                                                    while ($course_row = mysqli_fetch_array($category_query)) {
                                                        $category_id = $course_row["id"];
                                                        $category_name = $course_row["name"];

                                                        if ($category == $category_id) {
                                                            echo "<option selected value='$category_id'>$category_name</option>";
                                                        } else {
                                                            echo "<option value='$category_name'>$category_name</option>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="prof_point">Professional Points (Optional)</label>
                                            <input name="prof_point" class="form-control" type="text" id="prof_point"
                                                   value="<?= $prof_point ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="speciality">Speciality</label>
                                            <input name="speciality" class="form-control" type="text" id="speciality"
                                                   value="<?= $speciality ?>" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="cost">Course Cost</label>
                                            <input name="cost" class="form-control" type="text" id="cost"
                                                   value="<?= $cost ?>" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Course Description</label>
                                            <textarea name="description" class="form-control" type="text"
                                                      id="mytextarea" / required><?= $description ?></textarea>
                                        </div>
                                        <input type="submit" name="updateButton" id="updateButton" value="Update course"
                                               class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script src="plugins/select2/select2.full.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<script>
    $(function () {
        $(".select2").select2();
    });
</script>
</body>
</html>
