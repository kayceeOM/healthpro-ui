<?php
//connecting to database
include('../storescripts/connect_to_mysql.php');
include('../storescripts/crypto.php');
session_start();
if (!isset($_SESSION["admin_manager"])) {
    echo " <script>window.location='login.php';</script>";
    exit();
}
?>

<?php
// Delete Item Question to Admin, and Delete Product if they choose
if (isset($_GET['deleteid'])) {
    echo "<script>
         var r = confirm('delete Location?');
          if (r == true) {
              window.location='all_locations.php?yesdelete=" . $_GET['deleteid'] . "';
          } else {
              window.location='all_locations.php';
          }
 </script>";
    // echo 'Do you really want to delete Course with ID of ' . $_GET['deleteid'] . '? <a href="' . $_GET['deleteid'] . '">Yes</a> | <a href="all_courses.php">No</a>';
    exit();
}
if (isset($_GET['yesdelete'])) {
    // remove item from system and delete its picture
    // delete from database
    $id_to_delete = $_GET['yesdelete'];
    $sql = mysqli_query($conn, "DELETE FROM training_loc WHERE id='$id_to_delete' LIMIT 1") or die (mysqli_error($conn));

    echo " <script>window.location='all_locations.php';</script>";
}

?>

<?php
if (isset($_POST['insertButton'])) {
    $name = $_POST['name'];
    $address = $_POST['address'];

    $insertquery = "Insert into training_loc (name, location) values ('" . $name . "', '" . $address . "')";
    //echo  $insertquery;  
    $insertLocation = mysqli_query($conn, $insertquery) or die(mysqli_error($conn));
    if ($insertLocation) {
        echo " <script>alert('Location has been added');</script>";
        echo " <script>window.location='all_locations.php';</script>";
    } else {
        echo " <script>alert('Error! Location not added');</script>";
    }
}

?>

<?php
// This block grabs the whole list for viewing
$user = $_SESSION['admin_manager'];
$location_list = "";
$shop_locations = mysqli_query($conn, "select * from training_loc") or die(mysqli_error($conn));
$productCount = mysqli_affected_rows($conn);
if ($productCount > 0) {
    while ($row = mysqli_fetch_array($shop_locations)) {
        $id = $row["id"];
        $name = $row["name"];
        $address = $row["location"];

        //$CourseID = encrypt($id);
        $location_list .= " 
 
				<tr>
					<td>$name</td>
					<td>$address</td>
					<td align='center'>
            <div class='btn-group'>
              <a href='edit_locations.php?pid=$id' style='margin:0 auto;' class='btn btn-primary text-center'>
                <span class='fa fa-cog'></span>
              </a>
            </div>
          </td>
					<td align='center'>
            <div class='btn-group text-center'>
              <a href='all_locations.php?deleteid=$id' style='margin:0 auto;' class='btn btn-primary'>
                <span class='fa fa-trash'></span>
              </a>
            </div>
				  </tr>

			 ";

    }
} else {
    //$Course_list = "You have no Courses listed in your store yet";
}
?>

<?php
if (isset($_POST["export"])) {
    include('simple_html_dom.php');
    $header = '     <tr>
                        <th>Location Name</th>
                        <th>Location Address</th>
                      </tr>';
    $html = str_get_html($header . "" . $location_list); // give this your HTML string

    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=locations.csv');

    $fp = fopen("php://output", "w");

    foreach ($html->find('tr') as $element) {
        $td = array();
        foreach ($element->find('th') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }

        $td = array();
        foreach ($element->find('td') as $row) {
            if (strpos(trim($row->class), 'actions') === false && strpos(trim($row->class), 'checker') === false) {
                $td [] = $row->plaintext;
            }
        }
        if (!empty($td)) {
            fputcsv($fp, $td);
        }
    }

    fclose($fp);
    exit;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HealthPro | All Locations</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
    <!-- <script>
    tinymce.init({
      selector: '#mytextarea',
    });
   
    </script> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once("template_header.php") ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Locations

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Locations</a></li>
                <li class="active">All Locations</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">

                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <div class="col-xs-3 pull-left">
                                    <form name="exportform" method="post" action="all_locatons.php">
                                        <input type="submit" name="export" Value="Export Form"
                                               class="btn btn-sm btn-default btn-flat"/>
                                    </form>
                                </div>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Location Name</th>
                                        <th>Location Address</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php echo $location_list; ?>
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div><!-- /.box -->

                </div><!-- /.col -->

                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h1>
                                New Category
                            </h1>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- /.form-group -->
                                    <form id="form1" name="form1" method="post" enctype="multipart/form-data"
                                          action="all_locations.php">
                                        <div class="form-group">
                                            <label for="name">Location Name</label>
                                            <input name="name" class="form-control" type="text" id="name"
                                                   placeholder="Location Name" / required>
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Location Address</label>
                                            <textarea name="address" class="form-control" type="text" id="mytextarea" /
                                            required></textarea>
                                        </div>
                                        <input type="submit" name="insertButton" id="insertButton"
                                               value="Insert Location"
                                               class="btn btn-sm btn-default btn-flat pull-right">
                                    </form>

                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.col -->


            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <?php include_once("footer.php") ?>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.min.js"></script>

<script src="plugins/select2/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>
