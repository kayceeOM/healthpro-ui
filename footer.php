<section id="footer">
    <div class="container">
        <div class="row">
            <h2 class="text-center">Subscribe to Our Newsletter</h2>
            <p class="sub">Get all the latest news and promotions from us and our partners</p>

            <div class="clearfix"></div>


            <div class="col-md-8 col-md-offset-2">
                <form action="subscribe.php" method="POST">
                    <div class="row">
                        <div class="col-md-5 pad-xs">
                            <input name="name" type="text" placeholder="Full Name" class="form-control input" required>
                        </div>
                        <div class="col-md-5 pad-xs">
                            <input name="email" type="text" placeholder="Email Address" class="form-control input"
                                   required>
                        </div>
                        <div class="col-md-2 pad-xs">
                            <input type="submit" value="Subscribe" class="button">
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            </form>

            <hr>
        </div>

        <div class="row links">
            <div class="col-md-8 left no-pad">
                <a href="#" class="link">Services</a>
                <a href="#" class="link">Payment Options</a>
                <a href="#" class="link">FAQs</a>
                <a href="#" class="link">Resources</a>
                <a href="/admin" class="link">Admin Login</a>
            </div>
            <div class="col-md-4 right no-pad">
                &copy; Copyright &bull; HealthPro, 2016.
            </div>
        </div>
    </div>
</section>

        