<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
include_once("Paystack.php");
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
session_start();
if (!isset($_SESSION["user_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}
//echo phpinfo();
$email = decrypt($_SESSION["user_manager"]);
// Initialize as required by your installation
$results = $mysqli->prepare("SELECT user_id, unique_id FROM transaction where user_id='$email' order by date_created desc limit 1");
$results->execute(); //Execute prepared Query
$results->bind_result($user_id, $unique_id); //bind variables to prepared statement

while ($results->fetch()) { //fetch values

}

$curl = curl_init();
$customer = array();
$authorization = array();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($unique_id),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => [
        "accept: application/json",
        "authorization: Bearer sk_test_6f0fca8cb31021d73d4902f9d9fff89467c3854d",
        "cache-control: no-cache"
    ],
));
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($curl);
$err = curl_error($curl);

if($err){
    // there was an error contacting the Paystack API
    die('Curl returned error: ' . $err);
}

// $tranx = json_decode($response);
// print_r($tranx);
// $paystack = new Paystack('sk_test_bffb194e8fca14a5ea46e270ae7126618dcefb86');
// //echo $unique_id;
// $reference =  Session::get('reference');
// $responseObj = $paystack->transaction->verify(["reference"=>"$reference"]);
// $responseObj = json_encode($responseObj);

$responseObj = json_decode($response, true);
//
//$paystack = new Paystack('sk_test_6f0fca8cb31021d73d4902f9d9fff89467c3854d');
////echo $unique_id;
//$responseObj = $paystack->transaction->verify(["reference" => "$unique_id"]);
//$responseObj = json_encode($responseObj);
//$response = json_decode($responseObj, true);
foreach ($responseObj as $key => $value) {
    # code...
    if ($key == 'data') {

        $amount = ($value["amount"] / 100);
        $currency = $value["currency"];
        $transaction_date = $value["transaction_date"];
        $status = $value["status"];
        $reference = $value["reference"];

        $customer = $value["customer"];
        $first_name = $customer["first_name"];
        $last_name = $customer["last_name"];
        $email = $customer["email"];

        $authorization = $value["authorization"];
        $bank = $authorization["bank"];

        // echo "amount".$amount;
        // echo "<br/>currency".$currency;
        // echo "<br/>transaction_date".$transaction_date;
        // echo "<br/>status".$status;
        // echo "<br/>reference".$reference;
        // echo "<br/>first_name".$first_name;
        // echo "<br/>last_name".$last_name;
        // echo "<br/>email".$email;
        // echo "<br/>bank".$bank;

        $updateQuery = "update transaction set user_id ='$email', transaction_date='$transaction_date', amount='$amount', currency='$currency', bank='$bank', status='$status' where unique_id='$reference'";
        //echo  $insertquery;
        $updateUnique = mysqli_query($conn, $updateQuery) or die(mysqli_error($conn));

        if ($status == 'success') {
            $updateQuery = "update classroom set payment_status='paid' where unique_id='$reference'";
            //echo  $insertquery;
            $updateUnique = mysqli_query($conn, $updateQuery) or die(mysqli_error($conn));

            if ($updateUnique) {

                $results = $mysqli->prepare("SELECT courses.id, courses.name, courses.category from courses join classroom on courses.id = classroom.course_id where user_id='$email' and payment_status='paid' and unique_id=$unique_id limit 1");
                $results->execute(); //Execute prepared Query
                $results->bind_result($course_id, $course_name, $course_category); //bind variables to prepared statement

                while ($results->fetch()) { //fetch values

                }
                # code...
                $template = '
                        <!doctype html>
                        <html>
                          <head>
                            <meta name="viewport" content="width=device-width" />
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                            <title>Payment Successful</title>
                            <style>
                              /* -------------------------------------
                                  GLOBAL RESETS
                              ------------------------------------- */
                              img {
                                border: none;
                                -ms-interpolation-mode: bicubic;
                                max-width: 100%; }

                              body {
                                background-color: #f6f6f6;
                                font-family: sans-serif;
                                -webkit-font-smoothing: antialiased;
                                font-size: 14px;
                                line-height: 1.4;
                                margin: 0;
                                padding: 0; 
                                -ms-text-size-adjust: 100%;
                                -webkit-text-size-adjust: 100%; }

                              table {
                                border-collapse: separate;
                                mso-table-lspace: 0pt;
                                mso-table-rspace: 0pt;
                                width: 100%; }
                                table td {
                                  font-family: sans-serif;
                                  font-size: 14px;
                                  vertical-align: top; }

                              /* -------------------------------------
                                  BODY & CONTAINER
                              ------------------------------------- */

                              .body {
                                background-color: #f6f6f6;
                                width: 100%; }

                              /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
                              .container {
                                display: block;
                                Margin: 0 auto !important;
                                /* makes it centered */
                                max-width: 580px;
                                padding: 10px;
                                width: 580px; }

                              /* This should also be a block element, so that it will fill 100% of the .container */
                              .content {
                                box-sizing: border-box;
                                display: block;
                                Margin: 0 auto;
                                max-width: 580px;
                                padding: 10px; }

                              /* -------------------------------------
                                  HEADER, FOOTER, MAIN
                              ------------------------------------- */
                              .main {
                                background: #fff;
                                border-radius: 3px;
                                width: 100%; }

                              .wrapper {
                                box-sizing: border-box;
                                padding: 20px; }

                              .footer {
                                clear: both;
                                padding-top: 10px;
                                text-align: center;
                                width: 100%; }
                                .footer td,
                                .footer p,
                                .footer span,
                                .footer a {
                                  color: #999999;
                                  font-size: 12px;
                                  text-align: center; }

                              /* -------------------------------------
                                  TYPOGRAPHY
                              ------------------------------------- */
                              h1,
                              h2,
                              h3,
                              h4 {
                                color: #000000;
                                font-family: sans-serif;
                                font-weight: 400;
                                line-height: 1.4;
                                margin: 0;
                                Margin-bottom: 30px; }

                              h1 {
                                font-size: 35px;
                                font-weight: 300;
                                text-align: center;
                                text-transform: capitalize; }

                              p,
                              ul,
                              ol {
                                font-family: sans-serif;
                                font-size: 14px;
                                font-weight: normal;
                                margin: 0;
                                Margin-bottom: 15px; }
                                p li,
                                ul li,
                                ol li {
                                  list-style-position: inside;
                                  margin-left: 5px; }

                              a {
                                color: #3498db;
                                text-decoration: underline; }

                              /* -------------------------------------
                                  BUTTONS
                              ------------------------------------- */
                              .btn {
                                box-sizing: border-box;
                                width: 100%; }
                                .btn > tbody > tr > td {
                                  padding-bottom: 15px; }
                                .btn table {
                                  width: auto; }
                                .btn table td {
                                  background-color: #ffffff;
                                  border-radius: 5px;
                                  text-align: center; }
                                .btn a {
                                  background-color: #ffffff;
                                  border: solid 1px #3498db;
                                  border-radius: 5px;
                                  box-sizing: border-box;
                                  color: #3498db;
                                  cursor: pointer;
                                  display: inline-block;
                                  font-size: 14px;
                                  font-weight: bold;
                                  margin: 0;
                                  padding: 12px 25px;
                                  text-decoration: none;
                                  text-transform: capitalize; }

                              .btn-primary table td {
                                background-color: #3498db; }

                              .btn-primary a {
                                background-color: #3498db;
                                border-color: #3498db;
                                color: #ffffff; }

                              /* -------------------------------------
                                  OTHER STYLES THAT MIGHT BE USEFUL
                              ------------------------------------- */
                              .last {
                                margin-bottom: 0; }

                              .first {
                                margin-top: 0; }

                              .align-center {
                                text-align: center; }

                              .align-right {
                                text-align: right; }

                              .align-left {
                                text-align: left; }

                              .clear {
                                clear: both; }

                              .mt0 {
                                margin-top: 0; }

                              .mb0 {
                                margin-bottom: 0; }

                              .preheader {
                                color: transparent;
                                display: none;
                                height: 0;
                                max-height: 0;
                                max-width: 0;
                                opacity: 0;
                                overflow: hidden;
                                mso-hide: all;
                                visibility: hidden;
                                width: 0; }

                              .powered-by a {
                                text-decoration: none; }

                              hr {
                                border: 0;
                                border-bottom: 1px solid #f6f6f6;
                                Margin: 20px 0; }

                              /* -------------------------------------
                                  RESPONSIVE AND MOBILE FRIENDLY STYLES
                              ------------------------------------- */
                              @media only screen and (max-width: 620px) {
                                table[class=body] h1 {
                                  font-size: 28px !important;
                                  margin-bottom: 10px !important; }
                                table[class=body] p,
                                table[class=body] ul,
                                table[class=body] ol,
                                table[class=body] td,
                                table[class=body] span,
                                table[class=body] a {
                                  font-size: 16px !important; }
                                table[class=body] .wrapper,
                                table[class=body] .article {
                                  padding: 10px !important; }
                                table[class=body] .content {
                                  padding: 0 !important; }
                                table[class=body] .container {
                                  padding: 0 !important;
                                  width: 100% !important; }
                                table[class=body] .main {
                                  border-left-width: 0 !important;
                                  border-radius: 0 !important;
                                  border-right-width: 0 !important; }
                                table[class=body] .btn table {
                                  width: 100% !important; }
                                table[class=body] .btn a {
                                  width: 100% !important; }
                                table[class=body] .img-responsive {
                                  height: auto !important;
                                  max-width: 100% !important;
                                  width: auto !important; }}

                              /* -------------------------------------
                                  PRESERVE THESE STYLES IN THE HEAD
                              ------------------------------------- */
                              @media all {
                                .ExternalClass {
                                  width: 100%; }
                                .ExternalClass,
                                .ExternalClass p,
                                .ExternalClass span,
                                .ExternalClass font,
                                .ExternalClass td,
                                .ExternalClass div {
                                  line-height: 100%; }
                                .apple-link a {
                                  color: inherit !important;
                                  font-family: inherit !important;
                                  font-size: inherit !important;
                                  font-weight: inherit !important;
                                  line-height: inherit !important;
                                  text-decoration: none !important; } 
                                .btn-primary table td:hover {
                                  background-color: #34495e !important; }
                                .btn-primary a:hover {
                                  background-color: #34495e !important;
                                  border-color: #34495e !important; } }

                            </style>
                          </head>
                          <body class="">
                            <table border="0" cellpadding="0" cellspacing="0" class="body">
                              <tr>
                                <td>&nbsp;</td>
                                <td class="container">
                                  <div class="content">

                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <span class="preheader">Thanks For Choosing ' . $course_name . '.</span>
                                    <table class="main">

                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class="wrapper">
                                          <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td>
                                                <p>Hi ' . $first_name . '' . $last_name . ',</p>
                                                <p>Thank you for joining our course: ' . $course_name . '.</p>
                                                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                                                  <tbody>
                                                    <tr>
                                                      <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                          <tbody>
                                                            <tr>
                                                              <td> <a href="http://applegreen.ennov8.com.ng/classroom.php?a=' . encrypt($course_id) . '" target="_blank">Go to Course</a> </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                                <h3>Invoice</h3>
                                                <p>

                                                </p>
                                                <p>Good luck!</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>

                                      <!-- END MAIN CONTENT AREA -->
                                      </table>

                                    <!-- START FOOTER -->
                                    <div class="footer">
                                      <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td class="content-block">
                                            <span class="apple-link">© Copyright • HealthPro, 2016.</span>
                                          <!--  <br> Dont like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>.-->
                                          </td>
                                        </tr>
                                      </table>
                                    </div>

                                    <!-- END FOOTER -->
                                    
                        <!-- END CENTERED WHITE CONTAINER --></div>
                                </td>
                                <td>&nbsp;</td>
                              </tr>
                            </table>
                          </body>
                        </html>

                        ';

                $user_email = $email;
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                $headers .= "From: Healthpro Support <support@healthpro.com>" . "\r\n";
                // message subject
                $subject = 'Healthpro Payment confirmation for ' . $course_name . '';

                $result = mail($user_email, $subject, $template, $headers);

            }
        } else {
            echo " <script>window.location='payment_failure.php';</script>";
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php include("header.php"); ?>

    <section id="payment">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="payment-card">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                <div class="icon">
                                    <i class="ion ion-checkmark"></i>
                                </div>

                                <h2>Payment Successful!</h2>
                                <p>Your payment was received successfully. You will receive an email containing your
                                    invoice and payment details.</p>

                                <a href="courses.php">Return to Courses &nbsp; <i class="ion-android-arrow-forward"></i></a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
    <?php include("chat.php"); ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/remodal.min.js"></script>
    <script src="js/chat.js"></script>
    <!-- <script src="js/main.js"></script> -->

</body>
</html>