<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
if (!isset($_SESSION["user_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}

$email = decrypt($_SESSION["user_manager"]);
$course_list = '';

$results = $mysqli->prepare("SELECT id, firstname, lastname, email, phone, image, credit_points FROM account where email='$email'");
$results->execute(); //Execute prepared Query
$results->bind_result($id, $firstname, $lastname, $email, $phone, $image, $credit_points); //bind variables to prepared statement

while ($results->fetch()) { //fetch values

}


$courses = $mysqli->prepare("SELECT classroom.course_id, courses.name, courses.image, (select count(*) from module join courses on module.course_id = courses.id) As modulecount from classroom join courses on classroom.course_id = courses.id where classroom.payment_status='paid' and classroom.user_id = '$email'");
$courses->execute(); //Execute prepared Query
$courses->bind_result($course_id, $name, $course_image, $modulecount); //bind variables to prepared statement
//$num_courses = $courses->
$count = 0;
while ($courses->fetch()) { //fetch values
    $count++;
    $course_list .= '
        <a href="classroom.php?a=' . encrypt($course_id) . '" >
        <div class="col-md-3">
                        <div class="course-card">
                            <div class="image" style="background-image: url(course_icons/' . $course_image . ')">
                                
                            </div>
                            <div class="details">
                                <h3>' . $name . '</h3>
                                <p>' . $modulecount . ' Module(s)</p>
                            </div>
                        </div>
                    </div>
        </a>
    ';


}
//$courses->free();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<?php include("profile_header.php"); ?>

<section id="profile-hero" style="background-image: url(img/profile-bg.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="profile-details">
            <div class="profile-image" style="background-image: url(img/users/<?php echo $image ?>)"></div>
            <h1 class="profile-name"><?php echo $firstname . " " . $lastname ?></h1>
            <p class="email"><?php echo $email ?></p>

            <a href="profile_edit.php" class="edit-btn">Edit Profile &nbsp; <i class="ion-edit"></i></a>
        </div>
    </div>
</section>

<section id="courses" class="course-page">
    <div class="container">
        <div class="row first-row">
            <div class="col-md-3">
                <h2>My Courses</h2>
            </div>
            <div class="col-md-3">
                <a href="#" class="button disabled"><?= $count ?> Course(s)</a>
            </div>
        </div>
        <!-- END First Row-->

        <div class="row courses-row">
            <?= $course_list ?>
        </div>
    </div>
</section>

<?php include("footer-min.php"); ?>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>