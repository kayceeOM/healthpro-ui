<?php
include_once('storescripts/connect_to_mysql.php');
include_once('storescripts/crypto.php');
$reply = '';
session_start();
if (isset($_SESSION["user_manager"])) {
    echo " <script>window.location='index.php';</script>";
    exit();
}


if(isset($_GET["a"])) {
    $email = decrypt($_GET["a"]);
}
elseif (isset($_POST["resetButton"])) {

    $email = $_POST["email"];
    echo $email;
    $pass = $_POST["pass"];
    $pass2 = $_POST["pass2"];

    if ($pass == $pass2) {
        $password = md5($pass);

        $update_password = "update account set password  = '$password' where email = '$email'";
        // insert into the database
        $update_pro = mysqli_query($conn, $update_password) or die(mysqli_error($conn));

        if ($update_pro) {
            $reply = '
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Your email has been reset. please <a href="/login.php">Login</a> to access your account
        </div>
        ';

        } else {
            $reply = '
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            An Error occured during reset, Pls contact support 
        </div>
        ';
        }//if user was not inserted properly
    }//if passwords match
    else {
        $reply = '
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Sorry! Passwords do not match 
    </div>
    ';
    }
}
else{
    echo "<script>window.open('login.php','_self')</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<?php include("header.php"); ?>
<section id="profile-edit" class="sign-up-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="edit-profile-card">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center">Enter new password</h2>
                            <form name="sign-up-form" method="post" enctype="multipart/form-data" action="reset.php"
                                  class="edit-profile-form">
                                <?php echo $reply; ?>
                                <input type="hidden" name="email" value="<?php echo $email; ?>">
                                <input name="pass" type="password" class="form-control input" placeholder="Password">

                                <input name="pass2" type="password" class="form-control input"
                                       placeholder="Confirm Password">

                                <input name="resetButton" type="submit" value="Reset Email" class="button full-width"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END First Row-->

    </div>
</section>

<?php include("footer-min.php"); ?>
</body>
</html>