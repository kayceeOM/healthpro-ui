<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');
session_start();
if (!isset($_SESSION["user_manager"])) {
    $_SESSION['payment-data'] = $_POST;
    $_SESSION['payment-error'];
    echo " <script>window.location='login.php';</script>";
}
if (isset($_POST['enroll'])) {
    $course_id = $_POST['course_id'];
    $course_name = $_POST['course_name'];
    $prof_point = $_POST['prof_point'];
    $cost = $_POST['cost'];
    $location = $_POST['location'];
} else {
    echo " <script>window.history.back()</script>";
}
?>

<?php

$unique = rand(1000000, 9999999);
$email = decrypt($_SESSION["user_manager"]);
$insertquery = "Insert into transaction (user_id, unique_id, date_created) values ('$email', $unique, now())";
//echo  $insertquery;
$insertUnique = mysqli_query($conn, $insertquery) or die(mysqli_error($conn));
$insertquery1 = "Insert into classroom (user_id, course_id, unique_id, location, payment_status, date_created) values ('$email', $course_id, '$unique', '$location', 'pending', now())";
//echo  $insertquery;
$insertUnique1 = mysqli_query($conn, $insertquery1) or die(mysqli_error($conn));

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php include("header.php"); ?>
    <section id="payment">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="card">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                <h2>Pay Here </h2>
                                <p>You're Paying for <span class="course-title"><?= $course_name; ?></span>.</p>

                                <div class="card-subsection card-subsection--price">
                                    <h3>Price</h3>
                                    <span class="price"><?= $cost; ?></span>
                                </div>

                                <form method="post">
                                    <script src="https://js.paystack.co/v1/inline.js"></script>
                                    <button type="button" class="button full-width" onclick="payWithPaystack()"> Pay
                                    </button>
                            </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>
</div>
</section>
<?php include("footer.php"); ?>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/chat.js"></script>
<script>

    function payWithPaystack() {
        var handler = PaystackPop.setup({
            key: 'pk_test_f9ffc8748c12856ce750454bae19b6b9486ca05a',
            email: '<?= decrypt($_SESSION["user_manager"]) ?>',
            amount: <?= $cost . "00" ?>,
            ref: "<?= $unique ?>",
            metadata: {
                custom_fields: [
                    {}
                ]
            },
            callback: function (response) {
                window.location = 'payment_success.php';
            },
            onClose: function () {
                alert('window closed');
            }
        });
        handler.openIframe();
    }
</script>
<!-- <script src="js/main.js"></script> -->

</body>
</html>