<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">

    <?php
    session_start();
    include("header.php"); ?>

    <section id="faqs">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Frequently Asked Questions</h2>
                <div class="col-sm-12 col-sm-offset-0">

                    <div class="row faqs">

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Does the course price include travel and accommodation?</h3>
                                <p>Course prices do not cover travel or accommodation costs. You are responsible for
                                    making your transportation and hotel arrangements. The training team will provide
                                    you with further information once you are successfully registered for a course.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Which payment methods are accepted?</h3>
                                <p>Applegreen accepts payment via ATM or online bank transfers, Verve, Visa, MasterCard,
                                    Discover, American Express, Diners, JCB, PIN debit cards with the Visa or MasterCard
                                    logo, Debit cards with the Visa or MasterCard logo and PayPal.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>How do I pay by online transfer?</h3>
                                <p>You may choose to pay by online transfer at any time after your registration.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>How do I register an employee on his/her behalf?</h3>
                                <p>We recommend that each registration be made individually, by the person attending the
                                    course or exam. However, you can register an employee on his/her behalf using a
                                    unique Key. Please contact us learning@applegreenconsult.com if you would like to
                                    request a unique Key.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>How do I make a change to a booking?</h3>
                                <p>It is not possible to make changes to a booking without cancelling it first. If you
                                    would like to register for a different course or session, withdraw from the booking
                                    you wish to change and register again. Please note that prices may vary between
                                    sessions and locations due to Location, prevailing exchange rates, and number of
                                    candidates left to fill a class.</p>
                                <p>
                                    Depending on the course category and specialty (Please view our Cancellation
                                    Policy), a booking can be cancelled up to 14 days before the course start date with
                                    a full refund. If you would like to make a change to your booking less than 14 days
                                    prior to the course start date, please contact us.
                                </p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>How do I cancel a booking?</h3>
                                <p>Depending on the category and specialty You can cancel a booking directly from your
                                    user account provided that the cancellation is made more than 14 days before the
                                    course start date.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Can I transfer my booked courses to a colleague should I have to cancel?</h3>
                                <p>You may transfer your registration to a colleague at no additional cost provided that
                                    your colleague attends the same class. You can request the transfer by e-mail at
                                    least 72 hours before the course start date. If you wish to transfer in less than 72
                                    hours please send an e-mail to learning@applegreenconsult.com</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Can a course be cancelled by Applegreen?</h3>
                                <p>In the event of insufficient enrolment, Applegreen reserves the right to cancel any
                                    course 7 days prior to course commencement, in which case your course fee is fully
                                    refundable. You will be advised on other locations close to you at which case you
                                    can transfer your booking. You may also choose to transfer your payment to the new
                                    dates. Applegreen will not be liable for any expenses incurred by course
                                    participants, e.g. airfare or hotel charges.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>If it is too late to be refunded for a course, can I apply my payment to another
                                    session?</h3>
                                <p>Your payment cannot be transferred to a future session if you cancel your order less
                                    than 14 days before the course start date. However, you can transfer your booking to
                                    a friend who shares the same profession to attend the course in your place at no
                                    additional cost.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Will I be refunded for a course I cannot attend in the case of an emergency?</h3>
                                <p>You will not be refunded under any circumstances for a cancellation made later than
                                    14 days prior to course commencement. Always notify Applegreen of a cancellation as
                                    soon as you face an emergency situation or visa problem.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Will I receive any materials prior to the course?</h3>
                                <p>Yes. For all courses requiring preparation, Applegreen provides all course materials
                                    upon payment for a course. However, you will bear the cost of delivery for the
                                    materials. You will be notified by email if we need more information before
                                    dispatching the materials.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Do Applegreen courses have pre-requisites? How will I know if I am prepared to
                                    register?</h3>
                                <p>For now, All Applegreen courses require participants to read, write and communicate
                                    in the language of instruction. Additional pre-requisites are listed within
                                    individual course descriptions on our website.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Do I get certificates at the end of all Applegreen courses?</h3>
                                <p>Licenses and certificates from certain authorities might be ready at a later date due
                                    to regulatory standards but Certificate of Participation will be issued by
                                    Applegreen Learning Center upon successful completion of every course or
                                    training.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>Will I have to repeat a course I failed to receive a Certificate of
                                    Participation?</h3>
                                <p>Yes. You must attend and successfully complete another session to receive a
                                    certificate. There is no discount on the price if you repeat a course.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>How can I become an Applegreen instructor?</h3>
                                <p>Please visit www.applegreenconsult.com/instructor to learn more on how to become one
                                    of our instructors.</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="col-sm-6">
                            <div class="faq">
                                <div class="icon">?</div>
                                <h3>What Payment Options does Apple Green offer ?</h3>
                                <p>Payments can be made to Applegreen Consult:</p>

                                <p>Bank: Guaranty Trust Bank (GTB)
                                    Account No: 0232856034<br/>
                                    OR<br/>
                                    Bank: First Bank Plc
                                    Account No: 2020928342</p>

                                <p>NB<br/>
                                    After payment, send your payment details including as depositor's name, slip number,
                                    payment date, amount paid and service required billing@applegreenconsult.com</p>
                            </div>
                        </div><!-- END Faq -->

                        <div class="clearfix"></div>


                    </div><!-- END Faqs -->
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>
<?php include("chat.php"); ?>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>