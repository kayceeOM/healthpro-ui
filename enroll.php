<?php
include('storescripts/connect_to_mysql.php');
include('storescripts/crypto.php');

session_start();
if (isset($_POST['course_id'])) {
    $reply = '';
    if (isset($_POST['coupon_btn'])) {
        $session_coupon = $_SESSION["coupon"];
        switch ($session_coupon) {
            case false:
                $coupon_name = $_POST['coupon_name'];
                $course_id = $_POST['course_id'];
                $course_name = $_POST['course_name'];
                $prof_point = $_POST['prof_point'];
                $cost = $_POST['cost'];
                $location = $_POST['location'];
                $query = "SELECT * FROM coupon where name = '$coupon_name'";
                $result = $conn->query($query);
                if ($result === false) {
                    //echo "false";
                    $reply = '
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                Coupon is not valid
                                            </div>
                                            ';
                } else {
                    //echo "true";
                    $result->data_seek(0);
                    while ($row = $result->fetch_assoc()) {
                        $type = $row["type"];
                        //echo "type ".$type;
                        $value = $row["value"];
                        switch ($type) {
                            case "percent":
                                $cost = $cost - (($value / 100) * $cost);
                                //echo $cost;
                                $_SESSION["coupon"] = true;
                                $reply = '
                                                        <div class="alert alert-success alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            Coupon has been applied
                                                        </div>
                                                        ';
                                break;
                            case "amount":
                                $cost = $cost - $value;
                                $_SESSION["coupon"] = true;
                                $reply = '
                                                        <div class="alert alert-success alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            Coupon has been applied
                                                        </div>
                                                        ';
                                break;
                            default:
                                $reply = '
                                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            Coupon is not valid
                                                        </div>
                                                        ';
                        }
                    }
                }

                break;
            case true:
                $coupon_name = $_POST['coupon_name'];
                $course_id = $_POST['course_id'];
                $course_name = $_POST['course_name'];
                $prof_point = $_POST['prof_point'];
                $cost = $_POST['cost'];
                $location = $_POST['location'];
                $reply = '
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                         A coupon has been used already
                                </div>
                                ';
                break;

        }
    } else {
        $course_id = $_POST['course_id'];
        $course_name = $_POST['course_name'];
        $prof_point = $_POST['prof_point'];
        $cost = $_POST['cost'];
        $location = $_POST['location'];
    }
} else {
    echo "<script>window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php include("header.php"); ?>
    <section id="payment">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="card">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                <h2>Course Enrollment</h2>
                                <p>You're enrolling for <span class="course-title"><?= $course_name; ?></span>.</p>

                                <div class="card-subsection card-subsection--price">
                                    <h3>Price</h3>
                                    <span class="price"><?= $cost; ?></span>
                                </div>

                                <!-- <div class="card-subsection">
                                    <h3>Date</h3>
                                    <span>12 September – 14 September, 2016.</span>
                                </div>

                                <div class="card-subsection">
                                    <h3>Time</h3>
                                    <span>9am – 4pm</span>
                                </div> -->

                                <!-- <div class="hr"></div> -->

                                <div class="card-subsection">
                                    <h3>Venue</h3>
                                    <span><?= $location; ?></span>
                                </div>

                                <form name="coupon_form" class="edit-profile-form" method="post" action="enroll.php">
                                    <?= $reply; ?>
                                    <div class="col-md-6">
                                        <input name="prof_point" type="hidden" value="<?= $prof_point ?> ">
                                        <input name="course_name" type="hidden" value="<?= $course_name ?>">
                                        <input name="course_id" type="hidden" value="<?= $course_id ?>">
                                        <input name="cost" type="hidden" value="<?= $cost ?>">
                                        <input name="location" type="hidden" value="<?= $location ?>">
                                        <input name="coupon_name" type="text" class="form-control input"
                                               placeholder="Enter Coupon" required="">
                                    </div>
                                    <div class="col-md-6">
                                        <input name="coupon_btn" type="submit" value="Apply Coupon"
                                               class="button full-width"/>
                                    </div>
                                </form>
                                &nbsp;
                                <div class="hr"></div>
                                <form name="payment_form" class="edit-profile-form" method="post" action="payment.php">
                                    <input name="prof_point" type="hidden" value="<?= $prof_point ?> ">
                                    <input name="course_name" type="hidden" value="<?= $course_name ?>">
                                    <input name="course_id" type="hidden" value="<?= $course_id ?>">
                                    <input name="cost" type="hidden" value="<?= $cost ?>">
                                    <input name="location" type="hidden" value="<?= $location ?>">

                                    <div class="card-subsection">
                                        <input name="enroll" type="submit" value="Complete Enrollment"
                                               class="btn btn-primary btn-lg"/>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("footer.php"); ?>
    <?php include("chat.php"); ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/remodal.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/chat.js"></script>
    <!-- <script src="js/main.js"></script> -->

</body>
</html>