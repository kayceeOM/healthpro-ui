<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <title>HealthPro</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/ionicons.css">
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="remodal-bg">
    <?php
    session_start();
    include("header.php"); ?>


    <section id="services">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Corporates</h2>

                <div class="services">

                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="service">
                            <p>Our expert trainers scope, customize and deliver health and safety related courses to
                                meet specific needs of your business. Our training is designed to specifically up skill
                                and motivate corporate teams. Our classes are delivered to a minimum of 5 people at a
                                location of your choice.
                            <p>We provide logistical arrangements for large departments with attractive discounts on
                                training and bundle services.</p>

                            <p>Why you should use in-house training:</p>

                            <ul>
                                <li>Save cost of lodging and travel materials</li>
                                <li>You want to train group of at least 5 people</li>
                                <li>To reduce risk of exposure of confidential information</li>
                                <li>Need to transfer skills quickly to a team of employees</li>
                            </ul>

                            <p>Search through our list of courses or contact us for advise at
                                learning@applegreenconsult.com
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include("footer.php"); ?>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/remodal.min.js"></script>
<script src="js/chat.js"></script>
</body>
</html>